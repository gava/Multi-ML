module type MBSML =
  sig
    
    type 'a par

    type 'a tree

    val mbsp_gid : unit -> int list

    val mbsp_pid : unit -> int

    val generate_dynamic_id : unit -> int

    val generate_static_id : unit -> int

    exception Invalid_processor of int

    val replicate : 'a -> 'a par

    val mkpar : (int -> 'a) -> 'a par

    val proj : 'a par -> (int -> 'a)

    val put : (int -> 'a) par -> (int -> 'a) par

    type 'a final
            
    val final_value : 'a -> 'b -> 'a

    val start_timing : unit -> unit
                                 
    val stop_timing : unit -> unit

    val get_cost : unit -> float   

  end


module type MACHINE_PARAMETERS =
  sig

    type 'a tree

    val architecture : (int * string * int * int * int * int) tree

    val architecture_array : (string * int * int * int * int) array

    val print_architecture : (string * int * int * int * int) array -> unit

    val get_level_color : int -> int

    val get_children_color : int -> int

    val get_parent_color : int -> int

    val get_siblings_color : int -> int

    val get_memory_color : int -> int

  end


module type COMM =
  sig
    
    (** Performs implementation-dependent initialization. *)
    val initialize : unit -> unit

    (** Performs implementation-dependent finalization. *)
    val finalize : unit -> unit

    (** Returns the GID of the host processor*)
    val gid : unit -> int list

    (** Returns MPI identifier of the host processor*)                    
    val pid : unit -> int

    (** Returns the sibling identifier of the host processor*)                                          
    val sid : unit -> int

    (** Returns the total number of siblings of the host processor*)                    
    val nb_siblings : unit -> int

    (** Returns the number of children of the host processor*)                    
    val nb_children : unit -> int

    (** Returns the list of children of the host processor*)                       
    val my_children : unit -> int list

    val hostname : unit -> string

    val node : unit -> bool

    val root : unit -> bool                 

    val send : 'a array -> 'a array

    val send_bytes_to_child_i : bytes -> int-> unit
                                               
    val send_bytes_to_children : bytes-> unit

    val send_int_to_children : int-> unit

    val recv_bytes_from_child_0 : unit -> bytes

    val gather_bytes_from_children : bytes -> int array -> unit

    val gather_int_from_children : int array -> unit

    val marshal : 'a -> bytes

    val unmarshal : bytes -> int -> 'a

    val mbsp_barrier : unit -> unit

    val children_barrier : unit -> unit

    val parent_barrier : unit -> unit                           

    val wtime : unit -> float

                          (*    val get_closures_info : unit -> (int * float)*)

  end
