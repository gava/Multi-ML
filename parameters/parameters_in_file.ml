type 'a tree = Node of ('a * 'a tree list)
	       | Leaf of 'a

(*Architecture*)
(*Sera inclus à l'aide d'un fichier de config à la ML*)
(*Forme de la construction de l'arbre : 
  (pid * Type archi * Niveau * couleur Sons * couleur Father * couleur brothers)*)
(*(pid * Type archi * Niveau * couleur Sons * couleur Father * couleur brothers)*)
let architecture : (int * string * int * int * int * int) tree =
  (*Processeur*)
  Node ((0,"S",0,1,0,1) , [
    (*Cœur0 avec 2 threads*)
    Node ((1,"S",1,2,1,2) , [ Leaf((3,"S",2,0,2,3));Leaf((4,"S",2,0,2,3)) (*;Leaf( () ) ;Leaf( () ) *)])
    ;
    (*Cœur1 avec 2 threads*)
    Node ((2,"S",1,3,1,2) , [Leaf((5,"S",2,0,3,4));Leaf((6,"S",2,0,3,4) ) ])
  ])

                           
let build_nbsons tr = 
  let cpt = ref 0 in
  let rec aux tr =
  begin 
    match tr with
    | Node(_,s) ->
      incr cpt;
      let res = List.map aux s in
      let sum = List.fold_left (+) 0 
	(List.map (fun x -> match x with Node(v,_) -> v | Leaf _ -> 1) res) in
      Node(sum,res)
    | Leaf _ ->
      incr cpt;
      Leaf(0)
  end in
  let res = aux tr in
  (res,!cpt)

let get_v tr =
  begin 
    match tr with
    | Node (v,_)-> v
    | Leaf v -> v
  end

let get_s tr =
  begin 
    match tr with
    | Node (_,s)-> s
    | Leaf _ -> []
  end


let sum_left lst i =
let rec aux lst i acc =
  if i = 0 then acc 
  else
    aux (List.tl lst) (i-1) (acc+List.hd lst) 
in aux lst i 0


let gid = ref []
       
let compute_gid archi pid =
  let rec f ar lst =
    begin
      match ar with
      | Node ((p,_,_,_,_,_),_) when p = pid -> lst
      | Node (_,sub) -> List.flatten (List.mapi (fun i x -> f x (lst@[i])) sub)
      | Leaf (p,_,_,_,_,_) when p = pid -> lst
      | Leaf _ -> []
    end
  in f archi [0]
       

(*Unused and useless ?*)
(* let architecture_to_array tr = *)
(*   let rec aux tr pid = *)
(*     begin *)
(*       match tr with *)
(*       | Node(v,s) -> *)
(* 	let me = pid in *)
(* 	(\* let nb_sons = List.length s in *\) *)
(* 	Printf.printf "Sns %i\n" me; *)
(* 	let res = List.mapi *)
(* 	  (fun i x -> *)
(* 	    let s_pid = (if i = 0 then 1 else (sum_left (List.map get_v s) i)) in *)
(* 	    aux x (me+i+s_pid)) s in *)
(* 	Node(me,res) *)
(*       | Leaf v -> *)
(* 	Printf.printf "Sns %i\n" pid; *)
(* 	Leaf(pid) *)
(*     end *)
(*   in aux tr 0 *)

(*Old to delete ?*)
(* let architecture_to_array (tr,sz) archi = *)
(*   let arr = Array.make sz (  ("",0,0,0,0)) in *)
(*   let rec aux tr arch pid len =  *)
(*     begin *)
(*       match (tr,arch) with *)
(*       | (Node(v,s),Node(v2,s2)) -> *)
(*       let me = pid in *)
(* 	let len = List.length s in *)
(* 	let res = List.mapi (fun i x ->  *)
(* 	let s_pid = (if i = 0 then 1 else (sum_left (List.map get_v s) i)+1) in *)
(* 	aux x (List.nth s2 i) (me + i+ s_pid) len) s in *)
(* 	arr.(pid) <- ("",pid,pid,pid,pid); *)
(* 	Node(pid,res) *)
(*       | (Leaf v,Leaf v2) -> *)
(* 	arr.(pid) <- v2; *)
(* 	Leaf(pid) *)
(*       | (_,_) -> *)
(* 	failwith("Malformed tree during init\n") *)
(*     end  *)
(*   in let v = aux tr archi 0 1 in (v,arr) *)
  
let architecture_to_array (_,sz) archi =
  let arr = Array.make sz (  ("",0,0,0,0)) in
  let rec aux arch = 
    begin
      match arch with
      | Node(v,s) ->
	let pid = (fun (a,_,_,_,_,_) -> a) v in
	let res = List.map (fun x -> aux x) s in
	arr.(pid) <- (fun (_,a,b,c,d,e) -> (a,b,c,d,e)) v;
	Node(pid,res)
      | Leaf v ->
	let pid = (fun (a,_,_,_,_,_) -> a) v in
	arr.(pid) <- (fun (_,a,b,c,d,e) -> (a,b,c,d,e)) v;
	Leaf(pid)
    end 
  in let _ = aux archi in arr

let architecture_array = architecture_to_array (build_nbsons architecture) architecture

let print_architecture a = 
  Printf.printf "Static architecture :\n";
  ignore (Array.mapi
  (fun i (a,x,y,z,w) ->
	    Printf.printf "=> pid %i -> A: %s, L:%i, S:%i, F:%i, B:%i\n" 
	      i a x y z w) 
  (a))

(* let get_father_color = 42 *)


let get_level_color p = ((fun (_,l,_,_,_) -> l) architecture_array.(p))
let get_children_color p = ((fun (_,_,s,_,_) -> s) architecture_array.(p))
let get_parent_color p = ((fun (_,_,_,f,_) -> f) architecture_array.(p)) 
let get_siblings_color p = ((fun (_,_,_,_,b) -> b) architecture_array.(p))
let get_memory_color p = (let m = (fun (m,_,_,_,_) -> m) architecture_array.(p) in if m = "D" then 1 else 0)
let init_gid p = gid := compute_gid architecture p
let get_gid () = !gid
