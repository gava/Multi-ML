external mpi_init : string array -> unit = "ocaml_mpi_init"

let argv () = Sys.argv

let initialize () = mpi_init (argv ()) ;

external finalize : unit -> unit = "ocaml_mpi_finalize"

external pid : unit -> int = "ocaml_mpi_pid"

let gid = Parameters_in_file.get_gid

external sid : unit -> int = "ocaml_mpi_sid"

external nb_siblings : unit -> int = "ocaml_mpi_nbsiblings"

external nb_children : unit -> int = "ocaml_mpi_nbchildren"

external hostname : unit -> string = "ocaml_mpi_hostname"

let node () = nb_children () <> 0

let root () = pid () = 0

let my_children () = 
  let rec from_to n1 n2 = if n1>n2 then [] else n1::(from_to (n1+1) n2) in
  from_to 0 (nb_children ()-1)

external alltoall_int_array:
    int array -> int array -> unit
      = "ocaml_mpi_alltoall_int"
      
let alltoall_int_array src dst =
  if Array.length dst <> Array.length src 
  then failwith "MPI.alltoall_int_array: array size mismatch"
  else alltoall_int_array src dst 

external alltoall_string:
  string -> int array -> string -> int array -> unit
  = "ocaml_mpi_alltoall"

let send data =
  if Array.length data <> (nb_siblings())
  then failwith ("Mpi.alltoall: wrong array size "^
		    (string_of_int(Array.length data))^
		    " instead of "^
		    (string_of_int (nb_siblings())));
  let buffers =
    Array.map (fun d -> Marshal.to_string d [Marshal.Closures]) data in
  (* Determine lengths of strings *)
  let sendlengths = Array.map String.length buffers in
  let total_len = Array.fold_left (+) 0 sendlengths in
  let send_buffer = Bytes.create total_len in
  let pos = ref 0 in
  for i = 0 to (nb_siblings()) - 1 do
    String.blit buffers.(i) 0 send_buffer !pos sendlengths.(i);
    pos := !pos + sendlengths.(i)
  done;
  let recvlengths = Array.make (nb_siblings()) 0 in
  (* Alltoall those lengths *)
  alltoall_int_array sendlengths recvlengths;
  let total_len = Array.fold_left (+) 0 recvlengths in
  (* Allocate receive buffer *)
  let recv_buffer = Bytes.create total_len in
  alltoall_string send_buffer sendlengths recv_buffer recvlengths;
  (* Build array of results *)
  let res0= Marshal.from_string recv_buffer 0 in
  let res = Array.make (nb_siblings()) res0 in
  let pos = ref 0 in
  for i = 1 to (nb_siblings()) - 1 do
    pos := !pos + recvlengths.(i - 1);
    res.(i) <- Marshal.from_string recv_buffer !pos
  done;
  res

external send_bytes_to_child_i : bytes -> int-> unit = "ocaml_mpi_distribute_job_to_child_i"

external send_bytes_to_children : bytes-> unit = "ocaml_mpi_distribute_job_to_children"

external send_int_to_children : int-> unit = "ocaml_mpi_distribute_id_to_children"

external recv_bytes_from_child_0 : unit -> bytes = "ocaml_mpi_recv_mfun_child"

external gather_bytes_from_children : bytes -> int array -> unit = "ocaml_mpi_gather_mfun_from_children"

(* external gather_bytes_to_father : bytes -> int -> unit = "ocaml_mpi_gather_mfun_to_father" *)

external gather_int_from_children : int array -> unit = "ocaml_mpi_gather_int_from_children"
                                                      
let marshal v =
  Marshal.to_bytes v [Marshal.Closures]

let unmarshal v pos =
  Marshal.from_bytes v pos

external mbsp_barrier : unit -> unit = "ocaml_mpi_mbsp_barrier"

external children_barrier : unit -> unit = "ocaml_mpi_barrier_children"

external parent_barrier : unit -> unit = "ocaml_mpi_barrier_parent"
    
external wtime : unit -> float = "ocaml_mpi_wtime"
