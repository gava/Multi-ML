module Make :
  functor (P : Mbsmlsig.MACHINE_PARAMETERS) ->
    functor (C: Mbsmlsig.COMM) ->
      Mbsmlsig.MBSML
      = 
  functor (Parameters : Mbsmlsig.MACHINE_PARAMETERS) ->
    functor (Comm : Mbsmlsig.COMM) ->
struct

  let _ = 
    begin 
      Comm.initialize();
      at_exit Comm.finalize
    end

  exception Invalid_processor of int

  type 'a par = int

  type 'a tree = 'a

  let mbsp_gid _ = Comm.gid ()
    
  let mbsp_pid _ = Comm.pid ()


  let __dynamic_id = ref 0
    
  let generate_dynamic_id _ = 
    let v = !__dynamic_id in
    (*Printf.printf "pid %i -> id %i\n" (mbsp_pid ()) v;*)
    incr __dynamic_id;v

  let __static_id = ref 0
    
  let generate_static_id _ = 
    let v = !__static_id in
    (* Printf.printf "pid %i -> id %i\n" (mbsp_pid ()) v; *)
    incr __static_id;v
                       
                       
  (*Primitives*)
  let replicate a = 

    let new_id = generate_dynamic_id () in
    let ma = Comm.marshal a in
    Comm.send_bytes_to_children ma;
    Comm.send_int_to_children new_id;
    (* Printf.printf "New id = %i by pid [%i]\n" new_id (mbsp_pid());  *)
    new_id

 (*   for i = 0 to (Comm.nb_children()-1) do
     let v=res_array.(i) in
      mpi_send_mfun_to_child_i  (Marshal.to_bytes (fun _ _ -> v) [Marshal.Closures]) (i+1)
   done 
 *)
      
  let mkpar f =
    (* for i = 0 to (Comm.nb_children()-1) do *)
    (*   Comm.send_bytes_to_child_i *)
    (*     (Comm.marshal (fun _ _ -> f i (\* v *\))) (i+1) *)
    (* done; *)
    
    let res_array = Array.init (Comm.nb_children()) (fun i -> f i) in 
    (*let res_array = Array.init (Comm.nb_children()) (fun i -> (fun _ _ -> f i)) in*)
    for i = 0 to (Comm.nb_children()-1) do
      let v = res_array.(i) in 
      Comm.send_bytes_to_child_i
	(Comm.marshal (fun _ -> v )) (i+1)
    done;
    let new_id = generate_dynamic_id () in
    Comm.send_int_to_children new_id;
    new_id
             
  (*Bad proj … But it works …*)
  (*let proj a =
    Comm.send_bytes_to_children "proj";
    let (id : int) = (Obj.magic a) in
    Comm.send_int_to_children id;
    let res_proj = Comm.recv_bytes_from_child_0 () in
    let (f_proj : int -> 'a) = Comm.unmarshal res_proj in
    f_proj*)
   

  let proj a =
    Comm.send_bytes_to_children "proj";
    let (id : int) = (Obj.magic a) in
    Comm.send_int_to_children id;
    let recvlengths = Array.make (Comm.nb_children ()+1) 42 in
    let _ = Comm.gather_int_from_children recvlengths in

    (*Init recv tab*)
    let total_length = Array.fold_left (+) 0 recvlengths in

    (*Init recv_buf*)
    let recv_buf = Bytes.create total_length in
    let res = Comm.gather_bytes_from_children recv_buf recvlengths in

    Comm.children_barrier();
    
    let res0 = Comm.unmarshal recv_buf 0 in
    let res = Array.make (Comm.nb_children()) res0 in
    
    let pos=ref 0 in
    for i = 1 to (Comm.nb_children()) do
      pos := !pos + recvlengths.(i-1);
      res.(i-1) <- Comm.unmarshal recv_buf !pos
    done;
    
    (* (fun i -> res.(i)) *)
    
    (*Useful ? *)
      (fun i ->
      if (0<=i) && (i<Comm.nb_children()) then
        res.(i)
      else
        raise (Invalid_processor i)
      )
   
      
  let put v =
    Comm.send_bytes_to_children "put";
    let (id : int) = (Obj.magic v) in
    Comm.send_int_to_children id;
    let new_id = generate_dynamic_id () in
    Comm.send_int_to_children new_id;
    (Obj.magic)(new_id)

  type 'a final = 'a

  let final_value a _ = a

  exception Timer_failure of string

  let bsp_time_start = ref 0.

  type timing_state = Running | Stopped

  let timing = ref Stopped

  let start_timing () = 
    if !timing=Stopped
    then 
      (bsp_time_start := Comm.wtime();
       timing := Running)
    else
      raise (Timer_failure "Timer is already running")
	
  let stop_timing () =
    if !timing=Running
    then 
      (bsp_time_start := (Comm.wtime()) -. (!bsp_time_start);
       timing:=Stopped)
    else
      raise (Timer_failure "Timer was not started!")

  let get_cost () = !bsp_time_start

end
