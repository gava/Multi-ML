all : multiml

multiml :
	echo "Make everything"
	make -C utils
	make -C parsing
	make tools -C compilation
	make -C typing
	make -C compilation
	make -C toplevel
	make -C interface
	make -C parameters
	make -C par/generic
	make -C par/mpi
#bin files
	cp compilation/mpi/mpi_mml_cc bin/mpi_mml_cc
	chmod u+x bin/mmlopt.mpi
	chmod u+x bin/mmlc.mpi


multiml_debug :
	echo "Make everything"
	make -C utils debug
	make -C parsing debug
	make tools -C compilation debug
	make -C typing debug
	make -C compilation debug
	make -C toplevel debug
	make -C interface debug 
	make -C parameters debug
	make -C par/generic debug
	make -C par/mpi debug

doc :	
	ocamldoc -html -charset utf8 -t "Multi-ML version 0.4.02.1" -I ./typing -I parsing ./typing/*.mli -d doc

install : multiml
	mkdir -p `./bin/multimlbindir`
	cp bin/* `./bin/multimlbindir`
	mkdir -p `./bin/multimllibdir`
	cp parameters/parameters_in_file.cm[i,x] `./bin/multimllibdir`
	cp parameters/parameters_in_file.o `./bin/multimllibdir`
	cp par/mpi/runtimempi.cm[i,x] `./bin/multimllibdir`
	cp par/mpi/runtimempi.o `./bin/multimllibdir`
	cp par/mpi/mpi.cm[i,x] `./bin/multimllibdir`
	cp par/mpi/mpi.o `./bin/multimllibdir`
	cp par/generic/parallel.cm[i,x] `./bin/multimllibdir`
	cp par/generic/parallel.o `./bin/multimllibdir`
	cp par/mpi/comm_mpi.[c,h] `./bin/multimllibdir`

clean :
	echo "Clean everything"
	rm -rf doc/*
	make clean -C utils
	make clean -C parsing
	make clean -C compilation
	make clean -C toplevel
	make clean -C interface
	make clean -C parameters
	make clean -C par/generic
	make clean -C par/mpi
	make clean -C typing
	find ./ -name "*~" -delete
	find ./ -name "#*#" -delete
	rm bin/mpi_mml_cc bin/multimlbindir

.PHONY: all multiml doc clean
