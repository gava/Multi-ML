(*_*)

open Types
open Constraints
open Typing_errors


let flag_debug = ref true
let debug s = if !flag_debug then Printf.printf "%s\n%!" s

exception Loc_unkn

let unsolved_constraints = ref []

let accept_locality l1 l2 =
  if !flag_debug then Printf.printf "----->Testing %s acc %s\n" (effect_to_string l1.loc) (effect_to_string l2.loc) ;
  begin
    match (l1.loc,l2.loc) with
    (*Warning matching order is important*)
    | (a,b) when a = b -> ECEmpty
    | (Dot,_) -> ECEmpty
    | (Local,Comm) -> ECEmpty
    | (Local,Bsp) -> raise (Wrong_Locality(Local,Bsp,l2.cexp_loc))
    | (Bsp,Dot) -> ECEmpty (*Right ???*)
    | (Bsp,Comm) -> raise (Wrong_Locality(Bsp,Comm,l2.cexp_loc))
    (*| (Bsp,Level _) -> failwith "accept_locality bsp level" (*accept_locality (build_chunk Bsp l1.cexp_loc) (build_chunk (find_loc (Level a)) l2.cexp_loc)*)*)
    | (Bsp,Multi) -> raise (Wrong_Locality(Bsp,Multi,l2.cexp_loc))
    | (Multi,Comm) -> ECEmpty
    | (Level _,Dot) -> ECEmpty
    | (_,Dot) -> ECEmpty
    | (Level _,_) -> (* ECAcceptloc(l1,l2); *)raise (Loc_unkn) 
    | (_,Level _) -> (* ECAcceptloc(l1,l2); *)raise (Loc_unkn)
    | _ -> failwith ("TODO : acc "^(effect_to_string l1.loc)^" "^(effect_to_string l2.loc))
  end

let accept_definability l1 l2 =
    if !flag_debug then Printf.printf "----->Testing %s def %s\n" (effect_to_string l1.loc) (effect_to_string l2.loc) ;
  begin
    match (l1.loc,l2.loc) with
    (*Warning matching order is important*)
    | (a,b) when a = b -> ECEmpty
    | (Seq,Multi) -> ECEmpty
    | (Bsp,Multi) -> ECEmpty
    | (Local,Comm) -> ECEmpty
    | (Comm,Local) -> ECEmpty
    | (Dot,_) -> ECEmpty (* sure ??*)
    | (_,Dot) -> ECEmpty (* sure ??*)
    | (Level _,_) -> raise (Loc_unkn)
    | (_,Level _) -> raise (Loc_unkn)
    | _ -> failwith ("TODO : def "^(effect_to_string l1.loc)^" "^(effect_to_string l2.loc))
  end

let propagate_max l1 l2 =
  begin
    match (l1,l2) with
    | (a,b) when a = b -> l1
    (*Multi*)
    | (Multi,Bsp) -> Multi
    | (Multi,Local) -> Multi
    | (Multi,Comm) -> Multi
    | (Multi,Seq) -> Multi
    | (Multi,Dot) -> Multi
    | (Multi,Level _) -> raise (Loc_unkn)

    (*Bsp*)
    | (Bsp,Multi) -> Multi
    | (Bsp,Local) -> Bsp
    | (Bsp,Comm) -> Bsp
    | (Bsp,Seq) -> Bsp
    | (Bsp,Dot) -> Bsp
    | (Bsp,Level _) -> raise (Loc_unkn)(* (\* Bsp; *\)failwith "propagate_max (Bsp,λ)" *)

    (*Local*)
    | (Local,Multi) -> Multi
    | (Local,Bsp) -> Bsp
    | (Local,Comm) -> Local
    | (Local,Dot) -> Local
    | (Local,Level _) -> raise (Loc_unkn)

    (*Comm*)
    | (Comm,Multi) -> Multi
    | (Comm,Bsp) -> Bsp
    | (Comm,Local) -> Local
    | (Comm,Dot) -> Comm
    | (Comm,Level _) -> raise (Loc_unkn)

    (*Seq*)
    | (Seq,Multi) -> Multi
    | (Seq,Bsp) -> Bsp
    | (Seq,Dot) -> Seq
    | (Seq,Level _) -> raise (Loc_unkn)

    (*Dot*)
    | (Dot,Multi) -> Multi
    | (Dot,Bsp) -> Bsp
    | (Dot,Comm) -> Comm
    | (Dot,Local) -> Local
    | (Dot,Seq) -> Seq
    | (Dot,Level _) -> l2
       
    (*Level*)
    | (Level _,Dot) -> l1
    | (Level _,_) -> raise (Loc_unkn)
       
    | _ -> failwith ("TODO : max "^(effect_to_string l1)^" "^(effect_to_string l2))
  end 

let accept_type_in_loc t e =
  begin
    match (t,e.loc) with
    | (Type_par _,Comm) -> raise (Vector_Imbrication(e.cexp_loc))
    | (Type_tree _,Comm) -> failwith "Nope"
    | _ -> ECEmpty
  end
    
    
let rec seria e =
  begin
    match e.tau with
    | Type_var _ -> ()
    | Type_base("int",_) -> ()
    | Type_base("string",_) -> ()
    | Type_base("bool",_) -> ()
    | Type_base("unit",_) -> ()
    | Type_base _ -> raise (Non_Communicable(e.tau,e.fexp_loc))
    | Type_par _ -> raise (Non_Communicable(e.tau,e.fexp_loc))
    | Type_tree _ -> raise (Non_Communicable(e.tau,e.fexp_loc))
    | Type_arrow ((_,Local,_),_) -> raise (Non_Communicable(e.tau,e.fexp_loc))
    | Type_arrow ((_,Bsp,_),_) -> raise (Non_Communicable(e.tau,e.fexp_loc))
    | Type_arrow ((_,Multi,_),_) -> raise (Non_Communicable(e.tau,e.fexp_loc))
    | Type_arrow ((_,Seq,_),_) -> raise (Non_Communicable(e.tau,e.fexp_loc))
    | Type_arrow ((e1,_,e2),eff) -> seria (build_fragment e1 eff e.fexp_loc); seria (build_fragment e2 eff e.fexp_loc)
  end

let subst epsilon_eff_list =
  let rec do_subst chu =
    match chu.loc with
    | Level eff ->
       begin
         try
           List.assoc eff epsilon_eff_list
         with
         | Not_found -> chu
         | exn -> raise exn
       end
    | _ -> chu
  in
  do_subst

let subst_equations sigma c =
  List.map (function 
      | ECEmpty -> ECEmpty
      | ECEqual(chu1,chu2) -> ECEqual(sigma chu1, sigma chu2)
      (* | ECMax(chu1,chu2,chu3) ->  ECMax(chu1,sigma chu2,sigma chu3) *)

                                     
         (* ECAcceptloc(sigma chu1, sigma chu2) *)
      (* | ECAcceptloc_t(tau,chu) -> *)
      (*    accept_type_in_loc tau (sigma chu) *)
         (* ECAcceptloc_t(tau, sigma chu)      *)     
      (* | ECAcceptdef(chu1,chu2) -> *)
      (*    Printf.printf "TODO:ECAcceptdef"; ECEmpty *)
         (* ECAcceptdef(sigma chu1, sigma chu2) *)
      (* | ECMax(chu1,chu2,chu3) ->  *)
         (*let prop = {loc=propagate_max (sigma chu2).loc (sigma chu3).loc;cexp_loc=Tools.fake_location} in
         ECEqual(sigma chu1, prop)*)
      | ec -> ec
    ) c

let identite = fun typ -> typ 

let compose subst1 subst2 = fun typ -> subst1(subst2(typ)) 

let rec check_loc_cstr to_check infer_loc =
  begin
    match to_check with
    | ECAcceptloc(eff1,eff2) as c::c' ->
       begin
         try
           accept_locality (infer_loc eff1) (infer_loc eff2)
         with
         | Loc_unkn -> (* failwith "check_loc_cstr(Acc) : Loc_unkn" *)
            unsolved_constraints := [c]@(!unsolved_constraints);
            ECEmpty
         | exn -> raise exn
       end;
       check_loc_cstr c' infer_loc

    | ECAcceptloc_t(tau,eff2)::c' ->
       begin
         try
           accept_type_in_loc tau (infer_loc eff2)
         with
         | _ -> failwith "acct"
       end;
       check_loc_cstr c' infer_loc

    | ECAcceptdef(eff1,eff2) as c :: c' ->
       begin
         try
           accept_definability (infer_loc eff1) (infer_loc eff2)
         with
         | Loc_unkn -> (*failwith "check_loc_cstr(Def) : Loc_unkn"*)
            unsolved_constraints := [c]@(!unsolved_constraints);
            ECEmpty
         | exn -> raise exn
       end;
       check_loc_cstr c' infer_loc
                      
    | h::t -> check_loc_cstr t infer_loc

    | [] -> ()
  end


let order_e_constraints c =
  let rec do_it c_all c_eq c_max c_cond =
    begin
      match c_all with
      | [] -> (c_eq,c_max,c_cond)
      | ECEqual(_) as hd::tl ->
         do_it tl (c_eq@[hd]) c_max c_cond
      | ECMax(_) as hd::tl ->
         do_it tl c_eq (c_max@[hd]) c_cond
      | _ as hd::tl ->
         do_it tl c_eq c_max (c_cond@[hd])
    end
  in do_it c [] [] []
           
let rec l_mgu c_all =
  let rec l_mgu c =
    begin
      match c with
        [] -> debug "identite";
              identite
                
      | ECEqual({loc=e1;cexp_loc=_},{loc=e2;cexp_loc=_})::c' when e1 = e2 ->
         (* debug "ECEqual loc=loc"; *)
         (* if !flag_debug then Printf.printf "|->%s == %s\n" (effect_to_string e1) (effect_to_string e2); *)
         l_mgu c'

      | ECEqual({loc=Level eff1;cexp_loc=_},({loc=Level eff2;cexp_loc=_} as c2))::c'->
         (* debug "ECEqual labm=lamb"; *)
         (* if !flag_debug then Printf.printf "|->%s == %s\n" (eff1) (eff2); *)
         let s = subst [eff1,c2] in
         let se = subst_equations s c' in
         let mgu_se = l_mgu se in
         let res = compose mgu_se s in
         (* if !flag_debug then Printf.printf "|->%s == %s\n" (eff1) (effect_to_string e2); *)
         res 

      | ECEqual({loc=Level eff1;cexp_loc=_},({loc=e2;cexp_loc=_} as c2))::c'->
         (* debug "ECEqual lamb=loc"; *)
         let s = subst [eff1,c2] in
         let se = subst_equations s c' in
         let mgu_se = l_mgu se in
         let res = compose mgu_se s in
         (* if !flag_debug then Printf.printf "|->%s == %s\n" (eff1) (effect_to_string e2); *)
         res

      | ECEqual(({loc=e1;cexp_loc=_} as c1),{loc=Level eff2;cexp_loc=_})::c'->
         (* debug "ECEqual loc=lamb"; *)
         let s = subst [eff2,c1] in
         let se = subst_equations s c' in
         let mgu_se = l_mgu se in
         let res = compose mgu_se s in
         (* if !flag_debug then Printf.printf "|->%s == %s\n" (effect_to_string e1) (eff2); *)
         res

      | ECEqual({loc=Comm;cexp_loc=_},{loc=Local;cexp_loc=_})::c' ->
         debug "ECEqual c/l";
         l_mgu c'

      | ECEqual({loc=Local;cexp_loc=_},{loc=Comm;cexp_loc=_})::c' ->
         debug "ECEqual l/c";
         l_mgu c'
               
      | ECEqual(({loc=eff1;cexp_loc=_}) as c1,{loc=eff2;cexp_loc=_})::c' ->
         debug "ECEqual e=e'";
         begin
           match (eff1,eff2) with
           | (Dot,_) -> l_mgu c'
           | (_,Dot) -> l_mgu c'
           | _ -> raise (Loc_unification (eff1,eff2,c1.cexp_loc))
         end 
           
     | ECAcceptloc(_) as c::c' ->
         debug "ECAcceptloc";
         l_mgu c'

      | ECAcceptloc_t(_) as c::c' ->
         debug "ECAcceptloc_t";
         l_mgu c'

      | ECAcceptdef(_) as c::c' ->
         debug "ECAcceptdef";
         l_mgu c'

      | ECMax(eff1,eff2,eff3) as c::c' ->
         debug "ECMax";
         l_mgu c'
               
      | ECSeria(_) as c::c' ->
         debug "ECSeria";
         l_mgu c'

      | ECEmpty::c' ->
         debug "ECEmpty";
         l_mgu c'

      | _ -> failwith "unmatched"
    end
  in
  let c_eq,c_max,c_cond = order_e_constraints c_all in
  (* Printf.printf "-----------------------------\n"; *)
  (* Printf.printf "[[\n"; *)
  (* List.map (fun x -> Printf.printf "  %s\n" (e_constraints_to_string x)) c_eq; *)
  (* Printf.printf "]]\n"; *)
  (* Printf.printf "[[\n"; *)
  (* List.map (fun x -> Printf.printf "  %s\n" (e_constraints_to_string x)) c_max; *)
  (* Printf.printf "]]\n"; *)
  (* Printf.printf "[[\n"; *)
  (* List.map (fun x -> Printf.printf "  %s\n" (e_constraints_to_string x)) c_cond; *)
  (* Printf.printf "]]\n"; *)
  let reinject_list = ref [] in
  let rec enrich_infer infer c_eq c_max =
    begin
      match c_max with
      | [] -> infer
      | ECMax(eff1,eff2,eff3) as hd::tl ->
         begin
           try
             let max = propagate_max (infer eff2).loc (infer eff3).loc in
             let new_eq = ECEqual(eff1,build_chunk max (Tools.fake_location)) in
             let c_eq' = ([new_eq]@c_eq) in
             let infer' = l_mgu c_eq' in
             enrich_infer infer' c_eq' tl
           with
           | Loc_unkn ->
              begin
                (*Really ????*)
                (* Printf.printf "propagate %s,%s failed\n" (effect_to_string eff2.loc) (effect_to_string eff1.loc); *)
                (* Printf.printf "[[\n"; *)
                (* List.map (fun x -> Printf.printf "  %s\n" (e_constraints_to_string x)) c_eq; *)
                (* Printf.printf "]]\n"; *)
                
                (*Try to reinject the unsolved max once (max)*)
                if ( not (List.mem hd !reinject_list)) then(
                  reinject_list := (!reinject_list)@[hd];
                  enrich_infer infer c_eq (tl@[hd])
                )
                else(
                  enrich_infer infer c_eq tl
                )
              end
           | exn -> raise exn
         end;
    end
  in
  (*Building infer function by adding, successively, ECMax bindings*)
  let infer = enrich_infer (l_mgu c_eq) c_eq c_max in
  (*Check acc,def,seria,…*)
  let _ = check_loc_cstr c_cond infer in
  infer

        
  
(*     (\**Associate locality from tagged type*\) *)
let assoc_locality tau env_l = 
  let assoc_pair_loc l (loc_infer : Constraints.chunk -> Constraints.chunk) =
    try
      (loc_infer {loc=l;cexp_loc=Tools.fake_location}).loc
    with
    | Not_found -> failwith "assoc_pair_loc"
  in
  let rec do_it tau env_l =
    begin
      match tau with
      | Type_var (t,l) ->
	let l' = assoc_pair_loc l env_l in
	Type_var (t,l')
      | Type_base (t,l) ->
	let l' = assoc_pair_loc l env_l in
	Type_base (t,l')
      | Type_arrow ((left,lat_eff,right),eff) ->
	let lat_eff' = assoc_pair_loc lat_eff env_l in
	let eff' = assoc_pair_loc eff env_l in
	let left' = do_it left env_l  in
	let right' = do_it right env_l in
	Type_arrow ((left',lat_eff',right'),eff')
      | Type_pair ((p0,p1),l) ->
	let p0' = do_it p0 env_l in
	let p1' = do_it p1 env_l in
	let l = assoc_pair_loc l env_l in
	Type_pair ((p0',p1'),l)
      | Type_par (t,l) ->
	let t' = do_it t env_l in
	let l' = assoc_pair_loc l env_l in
	Type_par (t',l')
      | Type_tree (t,l) ->
	let t' = do_it t env_l in
	let l' = assoc_pair_loc l env_l in
	Type_tree (t',l')
    end
  in
  let res = do_it tau.tau env_l in
  build_fragment res tau.effect tau.fexp_loc

let unify_locality loc_env type_env frag =
  (* Printf.printf "Unify_Locality !\n%!"; *)
  (* Printf.printf "UNIFY_LOC[[\n"; *)
  (* List.map (fun x -> Printf.printf "  %s\n" (e_constraints_to_string x)) loc_env; *)
  (* Printf.printf "]]\n"; *)

  let loc_infer = l_mgu loc_env in
  let res = assoc_locality frag loc_infer in

  (* Printf.printf "Given frag: %s\n" (exprtype_to_string frag.tau); *)
  (* Printf.printf "Returned frag: %s\n" (exprtype_to_string res.tau); *)
  
  (* Printf.printf "-----------------\n%!"; *)
  (* frag *)
  res
