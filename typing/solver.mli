(**Constraint solving*)

open Constraints

(* (\**Unification de premier ordre*\) *)
(* val mgu : (fragment * fragment) list -> Types.exprtype -> Types.exprtype *)
(**The structure of an instance of the constraint solver
*)
type instance = {
  s : constraints list; (**Stack (Current constraint being processed)*)
  u : t_constraints list list; (**Unification constraints on types (Multi Environment)*)
  l : e_constraints list list; (**Unification constraints on effects (Multi Environment)*)
  c : constraints (**External constraints (Next constraints)*)
}

(**Try to match the type scheme of a given type*)
val carried_type_scheme : Constraints.constraints list ->
  Constraints.t_constraints list list -> Constraints.e_constraints list list -> Constraints.fragment -> Constraints.fragment
  
(**Constraint solver*)
val constraints_solver : instance -> instance

(* (\**Initial environment : +,-,… (~pervasive.multi-ml)*\) *)
(* val initial_env : Constraints.t_constraints list *)
