(**Type unification algorithm*)

open Constraints

(**First order unification*)
val mgu : (t_constraints) list -> fragment -> fragment

(**Generated localities*)
val gen_loc : e_constraints list ref list ref

val pop_gen_loc : unit -> unit
val add_head_gen_loc : unit ->unit
