(**Core type definitions*)

(**Types of expressions*)
type exprtype =
| Type_base of string * effect
| Type_var of string * effect
| Type_arrow of (exprtype * effect * exprtype) * effect
| Type_pair of (exprtype * exprtype ) * effect
| Type_par of exprtype * effect
| Type_tree of exprtype * effect

(**Effects*)
and effect =
| Delta
| Seq
| Local
| Comm
| Bsp
| Multi
| Level of string
| Dot

(**Comparison operator*)
val eq_exprtype_t : exprtype -> exprtype -> bool

(**Type to string operator*)
val exprtype_to_string : exprtype -> string

(**Type to string (without effect) operator*)
val exprtype_to_string_no_effect : exprtype -> string

(**Effect to string operator*)
val effect_to_string : effect -> string

(**Verbose effect to string operator *)
val effect_to_string_full : effect -> string

(**[expression] contains both core expressions and the location from the source file*)
type expression =
{
  expr: expression_t;
  p_loc: Location.t;
}

(**Simplified expression structure*)
and  expression_t =
| Var of string
| Const of int
| String of string
| Bool of bool
| Unit
| Op of string
| Ifthenelse of expression * expression * expression
| Fun of string * expression
| App of expression * expression
| Pair of expression * expression
| Let of string * expression * expression
| Letrec of string * string * expression * expression
(*Multi-ML*)
| Replicate of expression
| Proj of expression
| Open of string
| Copy of string
| Letmultifun of string * string * expression * expression
| Letmultitreefun of string * string * expression * expression
| Wherebody of expression * expression
| Mktree of expression
| At of string
| Multifun of string * expression
          
(**[expression_t] to string operator*)
val expression_t_to_string : expression_t -> string

(**[expression] constructor*)
val mk_expression : expression_t -> Location.t -> expression

(**Returns the identifier ([string]) of the type of an [exprtype]*)
val type_of_exprtype : exprtype -> string 
    
(**Returns the identifier ([effect]) of the locality of an [exprtype]*)
val loc_of_exprtype : exprtype -> effect

(**Returns the effect of an arrow type*)
val get_arrow_effect : exprtype -> effect

(**Returns a new [exprtype] with the given location*)
val change_loc_of_exprtype : exprtype -> effect -> exprtype
