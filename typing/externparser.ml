(*Ce programme permet de transformer un AST MultiML en AST générique intermédiaire. De ce nouvel AST, on pourra généré les différentes implémentations séquentielles, MPI, TCP …*)

let infile = ref ""

let usage = "usage: " ^ Sys.argv.(0) ^ " [-i filename]" ^ " [options]"

let speclist = [
  ("-i", Arg.Set_string infile, ": input BSML file");
]

let () =
  Arg.parse
    speclist
    (fun x -> raise (Arg.Bad ("Bad argument : " ^ x)))
    usage;;


let astout_multi_ml = "./multi_ml.ast"

let astout_seq = "./seq.ast";;


let ast_multi_ml =
  let file = !infile in 
  let channel = open_in file in
  let lexbuf = Lexing.from_channel channel in
  try
    (*let top = Parse.toplevel_phrase lexbuf in*)
    let result = Parse.implementation lexbuf in 
    let expr = Typing.pexp_expr_to_expression_t result in
    let expr_type = Typing.type_expression_t (List.hd expr) in
    let (final,(_,_)) = Typing.abcdize expr_type in
    Printf.printf "%s\n" (Types.exprtype_to_string final);
    Printf.printf "--> {%s}\n" (Types.exprtype_to_string expr_type);
    Printf.printf "with:\n";
    ignore (List.map (fun x -> Printf.printf "%s\n" (Constraints.e_constraints_to_string x)) !Locality_unification.unsolved_constraints)
  with
  | (Lexer.Error(Lexer.Unterminated_comment _, _) )->
    print_string "Unterminated_comment\n"
    ;exit 1
  | Syntaxerr.Error e ->
    print_string "Syntax Error\n";
    Syntaxerr.report_error Format.std_formatter e
    ;exit 1
  (*TODO : position, ligne, etc.*)
  | Parsing.Parse_error->
    ignore (print_string "Parsing Error\n");
    let loc = Location.curr lexbuf in
    ignore (raise(Syntaxerr.Error(Syntaxerr.Other loc)))
    ;exit 1
    (*Handle typing errors*)
  | Typing_errors.Unbound_Value (s,l) as ex -> 
     Typing_errors.handle_unbounded_values s l
  | Typing_errors.Wrong_Type (g,e,l) -> 
     Typing_errors.handle_wrong_type g e l
  | Typing_errors.Wrong_Locality (g,e,l) -> 
     Typing_errors.handle_wrong_locality g e l
  | Typing_errors.Vector_Imbrication l -> 
     Typing_errors.handle_vector_imbrication l
  | Typing_errors.Non_Communicable (e,l) -> 
     Typing_errors.handle_non_communicable e l
  | Typing_errors.Loc_unification (e1,e2,l) -> 
     Typing_errors.handle_loc_unification e1 e2 l                                   
;;


(* (\*Génération de l'ast générique*\) *)

(* let ast_seq = Ast.transform ast_multi_ml *)

(* let outfile = open_out astout_seq *)
(* let out_file_formatter = Format.formatter_of_out_channel outfile *)

(* let _ = *)
(* Printast.implementation out_file_formatter ast_seq; *)
(* flush outfile; *)
(* close_out outfile; *)
(* print_string ("ok\nAST Multi ML available in : "^ astout_seq ^"\n");flush *)
(* ;; *)

(* (\*Génération du code intermédiaire histoire de pouvoir debugger*\) *)
(* let printer = Pprintast_seq.string_of_structure ast_seq in print_string printer *)
(* let _ = Printf.printf "\n----------------\n" *)

