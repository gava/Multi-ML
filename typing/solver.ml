open Types
open Constraints
open Typing_errors
open Multi_pervasives


let flag_debug = ref false
let debug s = if !flag_debug then Printf.printf "%s\n" s

type instance = {
  s : constraints list;
  u : t_constraints list list;
  l : e_constraints list list;
  c : constraints
}

let print_inst {s;u;l=_;c} =
  let u = List.flatten u in
  if !flag_debug then
    if u=[] then 
      Printf.printf "%s ; [] ; %s\n---\n%!" 
	(constraints_to_string (List.hd s)) 
	(constraints_to_string c)
    else
      (Printf.printf "%s ;" (constraints_to_string (List.hd s)) ;
       ignore(List.map (function | TCEmpty -> failwith "TCEmpty"| TCEqual (a,b) | TCEqual_type (a,b) -> Printf.printf "%s=%s ∧ " (fragment_to_string a)(fragment_to_string b)) u) ;
       Printf.printf ";%s \n---\n%!"(constraints_to_string c));;

let carried_type_scheme full_stack env env_l x_frag =
  let env = List.flatten env in
  let x = x_frag.tau in
  if !flag_debug then(
    Printf.printf "----ALL_STACK----\n";
    ignore(List.map (fun x -> Printf.printf "%s\n" (constraints_to_string x)) full_stack);
    Printf.printf "----EOF ALL_STACK\n";
    Printf.printf "Carried_Type_Scheme:\n";
    Printf.printf "search %s in ALL STACK\n" (exprtype_to_string x));
  let rec carried_s full_stack =
    match full_stack with
    | (CAnd(CEmpty,_)) :: t -> debug "CAnd";
      carried_s t
    | (CExists(_,CEmpty)) :: t -> debug "CExist";
      carried_s t
    | (CLet(y,_,c)) :: t when (not(eq_exprtype_t y.tau x) && c != CEmpty)-> debug "CLet FX.T C";
      carried_s t
    | (CLet(y,_,CEmpty)) :: t when not(eq_exprtype_t y.tau x) -> debug "CLet y:TS.T []";
      if !flag_debug then Printf.printf ":> %s == %s \n" (exprtype_to_string y.tau) (exprtype_to_string x);
      carried_s t
    | (CLet(x',CVar(ts),CEmpty)) :: _ when (eq_exprtype_t x'.tau x) -> debug "CLet x:Tv.T []";
      if !flag_debug then Printf.printf "---> %s\n%!" (constraints_to_string (CVar(ts)));
      ts
    | (CLet(_,CTypescheme((CForall(_,CEmpty))::_,tarrow),_)) :: _ -> debug "CLet x:TS[EMPTY].T []";
                                                                     tarrow
    | (CLet(_,CTypescheme((CEmpty::_),t),_)) :: _ ->
       debug "CLet x:TS[EMPTY].T []";
       Printf.printf "%s\n" (exprtype_to_string t.tau);
         t
    | (CLet(x',CTypescheme(cts,fa),CEmpty)) :: _ when (eq_exprtype_t x'.tau x) -> debug "CLet x:TS.T []";
      let cts = List.hd cts in
      if !flag_debug then Printf.printf "--------> %s\n%!" (constraints_to_string (cts));
      let add_env =
	begin 
	  match cts with
	  | CForall(_,CSubtype (a,b)) -> TCEqual(a,b)
          | CEmpty -> TCEmpty
	  | _ -> failwith "fail match"
	end 
      in
      if !flag_debug then(
	Printf.printf "Run mgu on env :\n";
	Printf.printf "UNCOMMENT !!!!!!!!\n";
	ignore (List.map (function| TCEmpty -> failwith "TCEmpty" |TCEqual (a,b) | TCEqual_type(a,b) -> Printf.printf "%s=%s ∧ " (exprtype_to_string a.tau)(exprtype_to_string b.tau)) (env@[add_env]));
	Printf.printf "\nunify on %s\n" (exprtype_to_string fa.tau));
      let tmp_env_frag = List.map (fun x -> x) in
      let new_env = ((tmp_env_frag env)@[(add_env)]) in
      (* Old unification*)
      (*let new_env' = List.map( function | TCEqual(a,b)|TCEqual_type(a,b) -> (a,b)) new_env in*)
      (*
	let coconut = mgu new_env' fa in 
	if !flag_debug then (Printf.printf "-> I found %s\n" (fragment_to_string coconut));
      *)
      (*New unification*)
      (* let coconut = Type_unification.mgu new_env fa in *)
      let flat_gen_loc = List.map (fun x -> !x) !Type_unification.gen_loc in
      Printf.printf "----> COCONUT INFER WITH: [[\n";
      List.map (fun x -> Printf.printf "%s\n" (e_constraints_to_string x) ) (List.flatten (env_l@flat_gen_loc));
      let coconut = Unification.unify_tagged_type fa [new_env] (env_l@flat_gen_loc) in
      if !flag_debug then (Printf.printf "-> I found(v2): %s\n" (fragment_to_string coconut));
      (*EOF Hash_unification*)
      coconut
    (*let carried = try
      begin
      match cts with
      (*| CForall(_,CSubtype(Type_base a,_)) -> Type_base a
      | CForall(_,CSubtype(_,Type_base b)) -> Type_base b*)
      | CForall(_,CSubtype(Type_pair(a,b),_)) -> Type_pair (a,b)
      (*| CForall(_,CSubtype(Type_arrow(a,b),_)) -> Type_arrow (a,b)*)
      | CForall(_,CSubtype(_,Type_arrow(a,b))) -> Type_arrow (a,b)
      (*| CForall(_,CSubtype(_,b)) -> b (*or (_,b) -> b ???*)*)
      | _ -> failwith "Bad constraint inside type scheme ??!"
      end;
      with
      | _ -> fa
      in carried*)
    (* ;( *)
    (* 	Printf.printf "input good type (tv,tb,ta,tp):\n%!";  *)
    (* 	let ti = input_line stdin in *)
    (* 	match ti with *)
    (* 	| "tv" -> let a = input_line stdin in (Type_var a) *)
    (* 	| "tb" -> let a = input_line stdin in (Type_base a) *)
    (* 	| "ta" -> let a = input_line stdin in let b = input_line stdin in (Type_arrow (Type_var a,Type_var b))	 *)
    (* 	| "tp" -> let a = input_line stdin in let b = input_line stdin in (Type_pair (Type_var a,Type_var b))	 *)
    (* ) *)
    | [CEmpty] ->
      begin 
	try
	  if !flag_debug then (Printf.printf "Trying to find %s inside env …\n" (exprtype_to_string x)); 
	  (*Printf.printf "Env : \n";
	    ignore(List.map (function | TCEmpty -> failwith "TCEmpty"|TCEqual(a,b)|TCEqual_type(a,b) -> Printf.printf "(%s,%s)\n" (fragment_to_string a) (fragment_to_string b)) env);*)
	  begin
	    try
	      build_fragment (List.assoc (type_of_exprtype x) (List.map (function | TCEmpty -> failwith "TCEmpty" |TCEqual(a,b)|TCEqual_type(a,b) -> (type_of_exprtype a.tau,b.tau)) env)) Dot x_frag.fexp_loc
	    with 
	    | Not_found -> 
	  (*Case id is from pervasive*)
	      debug ((type_of_exprtype x)^"\n");
	      multi_pervasives_registered (type_of_exprtype x) (*Dot x_frag.fexp_loc*)
	  end

	with
	| Not_found -> raise (Unbound_Value (exprtype_to_string x,x_frag.fexp_loc))
      (*| _ -> failwith "Uncaught exception :'("*)
      end
    | _ -> failwith "carried_type_scheme : uncaught match"
  (*"carried_type_scheme : Empty Stack ! Bitch !" *)
  in carried_s full_stack

let add_queue lst elem =
  let old = 
    begin
      try
	(List.rev (List.tl (List.rev lst)))
      with
      | Failure _ -> []
    end
  in
  let young = 
    begin
      try
	([(List.hd (List.rev lst))@[elem]])
      with
      | Failure _ -> [[elem]]
    end 
  in old@young
(* (List.rev (List.tl (List.rev lst)))@([(List.hd (List.rev lst))@[elem]]) *)


let rec occurrence alpha tau =
  match tau with
  | Type_base _ -> false
  | Type_var (beta,_) -> alpha = beta
  (*| Type_scheme beta -> alpha = beta*)
  | Type_arrow ((tau1,_, tau2),_) -> occurrence alpha tau1 || occurrence alpha tau2
  | Type_pair ((tau1, tau2),_) -> occurrence alpha tau1 || occurrence alpha tau2
  | Type_par (tau,_) -> occurrence alpha tau
  | Type_tree (tau,_) -> occurrence alpha tau

(*Build locality equalities constraints from type equalities*)
(*let extract_loc tc =
  begin
    match tc with
    | TCEqual({tau=Type_var (alpha,alpha_loc);effect=_;fexp_loc=_} as tau0, tau1) when not(occurrence alpha tau1.tau) -> 
       let gen = ((loc_of_exprtype tau0.tau),(loc_of_exprtype tau1.tau)) in
       (ECEqual(build_chunk (fst gen) tau0.fexp_loc ,build_chunk (snd gen) tau1.fexp_loc))

    | TCEqual(tau0, {tau=Type_var (alpha,_) as tau1;fexp_loc=tau1_loc;_}) when not(occurrence alpha tau0.tau) -> 
       let gen = ((loc_of_exprtype tau0.tau),(loc_of_exprtype tau1)) in
       (ECEqual(build_chunk (fst gen) tau0.fexp_loc,build_chunk (snd gen) tau1_loc))
    | TCEqual(({tau=Type_arrow((tau1,l1, tau2),_);fexp_loc=_;_} as arr1),
	      ({tau=Type_arrow((tau1',l2,tau2'),_);fexp_loc=tau_loc;_} as arr2)) ->
       let gen = (l1,l2) in
       (ECEqual(build_chunk (fst gen) arr1.fexp_loc,build_chunk (snd gen) arr2.fexp_loc))
  end
 *)
    
let rec constraints_solver (i:instance) =
  if !flag_debug then ignore(input_line stdin);
  match i with
  | {s;u;l;c=CEqual(x,t)}->
     debug "S-Solve-Eq";
     let new_u = add_queue u (TCEqual(x,t)) in
     (*let new_l = add_queue l (extract_loc (TCEqual(x,t))) in*)
     let new_inst = {s;u=new_u;l=l;c=CTrue} in
     print_inst new_inst;
     constraints_solver new_inst
  | {s;u;l;c=CEqual_type(x,t)}->
     debug "S-Solve-Eq_Type";
     let new_u = add_queue u (TCEqual_type(x,t)) in
     let new_inst = {s;u=new_u;l=l;c=CTrue} in
     print_inst new_inst;
     constraints_solver new_inst
  | {s;u;l;c=CSubtype(x,t)} -> debug "S-Solve-Id";
    (*let new_c = union_find x s in*)
    let ts,l'' =
      if (function | Type_base _ | Type_pair _ -> false | _-> true) x.tau then
	(carried_type_scheme s u l x,
	 (ECEqual(build_chunk (loc_of_exprtype x.tau) x.fexp_loc,build_chunk (loc_of_exprtype t.tau) t.fexp_loc))
	)
      else (x,
	    ECEqual(build_chunk (loc_of_exprtype x.tau) x.fexp_loc,build_chunk (loc_of_exprtype t.tau) t.fexp_loc)
      )
    in
    if !flag_debug then Printf.printf "TS carried : %s\n" (exprtype_to_string ts.tau);
    let l' = 
      if (function | (Dot,_) | (_,Dot) -> true | _ -> false) (get_arrow_effect ts.tau,get_arrow_effect t.tau) then
	ECEmpty
      else 
	(ECEqual(build_chunk (get_arrow_effect ts.tau) ts.fexp_loc,build_chunk (get_arrow_effect t.tau) t.fexp_loc)(*;failwith "CSubtype ??"*) )
    in
    let new_l = add_queue (add_queue l l') l'' in
    let new_inst = {s;u;l=new_l(*l@[l';l'']*);c=CEqual(t,ts)} in (*ts,t*)
    print_inst new_inst;
    constraints_solver new_inst
  | {s;u;l;c=CSubtype_type(x,t)} -> debug "S-Solve-Id";
        (*let new_c = union_find x s in*)
    let ts =
      if (function | Type_base _ | Type_pair _ -> false | _-> true) x.tau then
	carried_type_scheme s u l x
      else x
    in
    if !flag_debug then Printf.printf "TS carried : %s\n" (exprtype_to_string ts.tau);
    let new_inst = {s;u;l;c=CEqual_type(t,ts)} in (*ts,t*)
    print_inst new_inst;
    constraints_solver new_inst
  | {s;u;l;c=CAnd(c1,c2)} -> debug "S-Solve-And";
    let new_s = [CAnd(CEmpty,c2)] in
    let new_inst = {s=new_s@(s);u;l;c=c1} in
    print_inst new_inst;
    constraints_solver new_inst
  | {s;u;l;c=CExists(_,c)} -> debug "S-Solve-Ex (to check)";
    constraints_solver {s;u;l;c}
  | {s;u;l;c=CLet(x,CVar(v),c)} -> debug "S-Solve-Let-Mono";
    let new_s = CLet(x,CVar(v),CEmpty) in
    let new_inst = {s=[new_s]@s;u;l;c} in
    print_inst new_inst;
    constraints_solver new_inst
  | {s;u;l;c=CLet(x,CTypescheme(d,t'),c)} ->
     debug "S-Solve-Let";
     Type_unification.add_head_gen_loc ();
     (* Printf.printf "----> gen_loc size(solve): ["; *)
     (* List.map (fun x -> Printf.printf "[%i] " (List.length !x)) !Type_unification.gen_loc; *)
     (* Printf.printf "]\n"; *)
     (* Printf.printf "----> l size(solve): ["; *)
     (* List.map (fun x -> Printf.printf "[%i] " (List.length x)) l; *)
     (* Printf.printf "]\n"; *)
     (* Printf.printf "----> u size(solve): ["; *)
     (* List.map (fun x -> Printf.printf "[%i] " (List.length x)) u; *)
     (* Printf.printf "]\n"; *)
     let new_s = CLet(x,CTypescheme([CEmpty],t'),c) in
     let new_inst = {s=new_s::s;u=u;l=l;c=(List.hd d)} in
     (if List.length d > 1 then failwith "S-Solve-Let") ;
     print_inst new_inst;
     constraints_solver new_inst
  | {s=CAnd(CEmpty,cstr)::s2;u;l;c=CTrue} ->
     debug "S-Pop-And";
     let new_inst = {s=s2;u;l;c=cstr} in
     print_inst new_inst;
     constraints_solver new_inst
  | {s=CLet(x,CTypescheme([CEmpty],ct),cC)::s2;u;l;c=CTrue}->
     debug "S-Pop-Let";
      (* Type_unification.pop_gen_loc ();  *)
      (* Type_unification.gen_loc := [ref[]];  *)
     (* Printf.printf "----> gen_loc size(pop): ["; *)
     (* List.map (fun x -> Printf.printf "[%i] " (List.length !x)) !Type_unification.gen_loc; *)
     (* Printf.printf "]\n"; *)
     (* Printf.printf "----> l size(pop): ["; *)
     (* List.map (fun x -> Printf.printf "[%i] " (List.length x)) l; *)
     (* Printf.printf "]\n"; *)
     (* Printf.printf "----> u size(pop): ["; *)
     (* List.map (fun x -> Printf.printf "[%i] " (List.length x)) u; *)
     (* Printf.printf "]\n"; *)
     (* Printf.printf "----> REMOVE: [[\n"; *)
     (* List.map (fun x -> Printf.printf "%s\n" (e_constraints_to_string x) ) (List.hd (List.rev l)); *)
     (* Printf.printf "]]\n"; *)
     (* debug ("TC :"^(exprtype_to_string ct.tau)^"\n"); *)
     let old_u = List.rev (List.tl (List.rev u)) in
     let old_l = List.rev (List.tl (List.rev l)) in
     let flat_gen_loc = List.map (fun x -> !x) !Type_unification.gen_loc in

     (* Printf.printf "----> INFER WITH in : [[\n ---------(l)-----------\n"; *)
     (* List.map (fun x -> Printf.printf "%s\n" (e_constraints_to_string x) ) (List.flatten l); *)
     (* Printf.printf "-------(gen_loc)-------:\n"; *)
     (* List.map (fun x -> Printf.printf "%s\n" (e_constraints_to_string x) ) (List.flatten flat_gen_loc); *)
     (* Printf.printf "]]\n"; *)
     (* (\* let x_type = Type_unification.mgu (List.flatten u) ct in *\) *)
     Printf.printf "----------------------------------------------------[%s\n" (exprtype_to_string ct.tau);

     let x_type = Unification.unify_tagged_type ct u (l@flat_gen_loc) in

     (*update x_type with cstrs*)
     let flat_gen_loc = List.map (fun x -> !x) !Type_unification.gen_loc in
     let to_add = (List.flatten l)@(List.flatten flat_gen_loc) in
     Printf.printf "-------(to add)-------:\n";
     List.map (fun x -> Printf.printf "%s\n" (e_constraints_to_string x) ) ((to_add));
     let x_type = {tau=x_type.tau;effect=x_type.effect;e_cstr=(List.flatten l)@(List.flatten flat_gen_loc);fexp_loc=x_type.fexp_loc} in
     (* List.map (fun x -> Printf.printf "%s\n" (e_constraints_to_string x) ) (List.flatten flat_gen_loc); *)
     Printf.printf "===================> Infered from solver [%s : %s] with :\n" (exprtype_to_string ct.tau) (exprtype_to_string x_type.tau) ;
     List.map (fun x -> Printf.printf "%s\n" (e_constraints_to_string x) ) x_type.e_cstr ;
     
     (*      Printf.printf "----> gen_loc size(pop): ["; *)
     (* List.map (fun x -> Printf.printf "[%i] " (List.length !x)) !Type_unification.gen_loc; *)
     (* Printf.printf "]\n"; *)
     (* Printf.printf "----> l size(pop): ["; *)
     (* List.map (fun x -> Printf.printf "[%i] " (List.length x)) l; *)
     (* Printf.printf "]\n"; *)
     (* Printf.printf "----> u size(pop): ["; *)
     (* List.map (fun x -> Printf.printf "[%i] " (List.length x)) u; *)
     (* Printf.printf "]\n"; *)
     (*Working …*)
     let new_s = CLet(x,CVar(x_type),CEmpty) in
     (*Try to insert locality constraints in TScheme *)
     (* let new_s = CLet(x,CTypescheme([CEmpty],[ECEqual(build_chunk (Level "λ9") Tools.fake_location, build_chunk (Level "λ10") Tools.fake_location)],x_type),CEmpty) in *)
     (* let new_s = CLet(x,CTypescheme([CEmpty],x_type),CEmpty) in  *)
     let new_inst = {s=new_s::s2;u=old_u;l=old_l;c=cC} in
     Type_unification.pop_gen_loc ();
     print_inst new_inst;
     constraints_solver new_inst
  | {s=CLet(_,_,CEmpty)::s2;u;l;c=CTrue as c} -> debug "S-Pop-Env";
    let new_inst = {s=s2;u;l;c} in
    print_inst new_inst;
    constraints_solver new_inst
  | {s=[CEmpty];u;l;c=CTrue} ->
    debug "Constraints solved !\n";
    {s=[CEmpty];u;l;c=CTrue}
  | {s;u;l;c=CForall(_,cc)} -> debug "Pop-For-All";
    constraints_solver {s;u;l;c=cc}
  (*TEST*)
  | {s;u;l;c=CMax(eq,x,y)} -> 
    let l' = ECMax(eq,x,y) in
    let new_l = add_queue l l' in
    debug ("------------------->"^(e_constraints_to_string l'));
    constraints_solver {s;u;l=new_l(*l@[l']*);c=CTrue}
  | {s;u;l;c=CAcceptloc_e(x,y)} -> 
    let l' = ECAcceptloc(x,y) in
    let new_l = add_queue l l' in
    debug ("------------------->"^(e_constraints_to_string l'));
    constraints_solver {s;u;l=new_l(*l@[l']*);c=CTrue}
  | {s;u;l;c=CAcceptloc_t(x,y)} -> 
    let l' = ECAcceptloc_t(x.tau,y) in
    let new_l = add_queue l l' in
    debug ("------------------->"^(e_constraints_to_string l'));
    constraints_solver {s;u;l=new_l(*l@[l']*);c=CTrue}
  | {s;u;l;c=CAcceptdef_e(x,y)} ->
    let l' = ECAcceptdef(x,y) in
    let new_l = add_queue l l' in
    debug ("------------------->"^(e_constraints_to_string l'));
    constraints_solver {s;u;l=new_l(*l@[l']*);c=CTrue}
  | {s;u;l;c=CEqual_e(x,y)} ->
    let l' = ECEqual(x,y) in
    let new_l = add_queue l l' in
    debug ("------------------->"^(e_constraints_to_string l'));
    constraints_solver {s;u;l=new_l(*l@[l']*);c=CTrue}
  | {s;u;l;c=CSeria(x)} ->
     failwith "seria@Solver:todo";
     let l' = ECSeria(x) in
     (* let tau_tmp = Type_unification.mgu (List.hd u) (build_fragment x Dot (Tools.fake_location_str "seria")) in *)
         let tau_tmp = Unification.unify_tagged_type (build_fragment x Dot (Tools.fake_location_str "seria")) ([List.hd u]) l in
     (* Printf.printf "%s\n" (exprtype_to_string tau_tmp.tau); *)
     (* Printf.printf "%s\n" (effect_to_string_full tau_tmp.effect); *)
     let _ = Locality_unification.seria tau_tmp in
     let new_l = add_queue l l' in
     debug ("------------------->"^(e_constraints_to_string l'));
     constraints_solver {s;u;l=new_l(*l@[l']*);c=CTrue}

  (*EOF TEST*)
  | i -> 
    Printf.printf "Fail inst : ";
    print_inst i;
    failwith "Match fail constraints_solver"
;;
