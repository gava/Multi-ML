open Constraints

(**Unify type and locality of a ginven tagged type*)
let unify_tagged_type tau env_t env_l =

  (* Printf.printf "----> gen_loc size(u): ["; *)
  (* List.map (fun x -> Printf.printf "[%i] " (List.length !x)) !Type_unification.gen_loc; *)
  (* Printf.printf "]\n"; *)
  (* Printf.printf "----> l size(u): ["; *)
  (* Printf.printf "[%i] " (List.length (List.flatten env_l)); *)
  (* Printf.printf "]\n"; *)
  (* Printf.printf "----> u size(u): ["; *)
  (* Printf.printf "[%i] " (List.length (List.flatten env_t)); *)
  (* Printf.printf "]\n"; *)
  
  let final_t = 
    Type_unification.mgu (List.flatten env_t) tau in


  (* Printf.printf "----> gen_loc size(u): ["; *)
  (* List.map (fun x -> Printf.printf "[%i] " (List.length !x)) !Type_unification.gen_loc; *)
  (* Printf.printf "]\n"; *)
  (* Printf.printf "----> l size(u): ["; *)
  (* Printf.printf "[%i] " (List.length (List.flatten env_l)); *)
  (* Printf.printf "]\n"; *)
  (* Printf.printf "----> u size(u): ["; *)
  (* Printf.printf "[%i] " (List.length (List.flatten env_t)); *)
  (* Printf.printf "]\n"; *)

  
  let flat_gen_loc = (List.map (fun x -> !x) (!Type_unification.gen_loc)) in
  let final_e_constraints = env_l@[List.flatten flat_gen_loc] in

  Printf.printf "[[(final_e_constraints)\n";
  let flat_gen_loc = List.flatten (List.map (fun x -> !x) (!Type_unification.gen_loc)) in
  List.map (fun x -> Printf.printf "[%s]\n " (Constraints.e_constraints_to_string (x))) (List.flatten final_e_constraints);
  Printf.printf "]]\n";
  

    Printf.printf "[[\n";
  List.map (fun x -> Printf.printf "  %s\n" (Constraints.e_constraints_to_string x)) (List.flatten env_l);
  Printf.printf "]]\n";

  
  Printf.printf "[[\n";
  List.map (fun x -> Printf.printf "  %s\n" (Constraints.e_constraints_to_string x)) (flat_gen_loc);
  Printf.printf "]]\n";
  
  let unified_loc = Locality_unification.unify_locality (List.flatten final_e_constraints) (List.flatten env_t) final_t in
  unified_loc 
