open Types
open Constraints
open Typing_errors

let flag_debug = ref true
let debug s = if !flag_debug then Printf.printf "%s\n" s

let (gen_loc : (e_constraints) list ref list ref) = ref [ref []];;
let pop_gen_loc () =
  (* List.map (fun x -> Printf.printf "[%i] " (List.length !x)) !gen_loc; *)
  (* Printf.printf "\n\n\nPop_Gen_Loc: was %i" (List.length !gen_loc); *)
  gen_loc := List.tl (!gen_loc)
  (* Printf.printf ", now %i\n\n\n" (List.length !gen_loc); *)
  (* ignore(List.map (fun x -> Printf.printf "[%i] " (List.length !x)) !gen_loc); *)
  (* Printf.printf "\n"  *)
           
let add_head_gen_loc () =
  (* List.map (fun x -> Printf.printf "[%i] " (List.length !x)) !gen_loc; *)
  (* Printf.printf "\n\n\nAdd_Head_Gen_Loc: was %i" (List.length !gen_loc); *)
  gen_loc := ([ref[]])@(!gen_loc)
  (* Printf.printf ", now %i\n\n\n" (List.length !gen_loc); *)
  (* ignore(List.map (fun x -> Printf.printf "[%i] " (List.length !x)) !gen_loc); *)
  (* Printf.printf "\n"  *)
                  
let rec add_to_gen_loc x =
  begin
  try
    List.hd !gen_loc := [x]@(!(List.hd !gen_loc))
  with
  | _ -> (* add_head_gen_loc (); add_to_gen_loc x *) failwith "add_to_gen_loc"
end  


let subst alpha_tau_list =
  let rec do_subst tau =
    match tau.tau with
    | Type_var (alpha,_) ->
       begin try
	   let res = List.assoc alpha alpha_tau_list in
	   (*let ll = (function | Type_var (_,x)|Type_arrow(_,x)|Type_base(_,x)|Type_pair(_,x)|Type_par(_,x)|Type_tree(_,x) -> x | _ -> failwith "subst type") res.tau in*)
	   let gen = ((loc_of_exprtype res.tau),(loc_of_exprtype tau.tau)) in
           (* Printf.printf "-*-*-*subst-*-*-*-* %s\n" (e_constraints_to_string (ECEqual(build_chunk (fst gen) res.fexp_loc ,build_chunk (snd gen) tau.fexp_loc))); *)
	   add_to_gen_loc (ECEqual(build_chunk (fst gen) res.fexp_loc ,build_chunk (snd gen) tau.fexp_loc)); 
	   res
         with Not_found ->
           tau
       end
    | Type_base _ -> tau
    | Type_arrow((tau1,e, tau2),eff) -> build_fragment (Type_arrow(((do_subst (build_fragment tau1 eff (Tools.fake_location_str "subst tau1"))).tau,e, (do_subst (build_fragment tau2 eff (Tools.fake_location_str "subst tau2"))).tau),eff)) eff (*Tools.fake_location_str "subst"*)tau.fexp_loc
    | Type_pair((tau1, tau2),eff) -> 
       build_fragment( Type_pair(((do_subst (build_fragment tau1 eff (Tools.fake_location_str "subst tau1"))).tau, (do_subst (build_fragment tau2 eff (Tools.fake_location_str "subst tau1"))).tau),eff)) eff (Tools.fake_location_str "subst")
    | Type_par (alpha,eff) -> build_fragment ( Type_par (((do_subst (build_fragment alpha eff (Tools.fake_location_str "alpha_tau_list Type_par"))).tau),eff)) eff tau.fexp_loc
    | Type_tree (alpha,eff) -> build_fragment ( Type_tree (((do_subst (build_fragment alpha eff (Tools.fake_location_str "alpha_tau_list Type_tree"))).tau),eff)) eff tau.fexp_loc
  (*| Type_arrow(tau1,e, tau2) -> 
    let t1 = {tau=tau1;cstr=tau.cstr;loc=tau.loc;fexp_loc=tau.fexp_loc} in
    let t2 = {tau=tau2;cstr=tau.cstr;loc=tau.loc;fexp_loc=tau.fexp_loc} in 
    let t = Type_arrow((do_subst t1).tau,e, (do_subst t2).tau) in empty_fragment t*)
  (*| Type_pair(tau1, tau2) -> Type_pair(do_subst tau1, do_subst tau2)
    | Type_par alpha -> Type_par (do_subst alpha)
    | _ -> failwith "alpha_tau_list : TODO all types"*)
  in
  do_subst


let compose subst1 subst2 = fun typ -> subst1(subst2(typ)) 

let subst_equations sigma c =
  List.map (function 
  | TCEmpty -> failwith "TCEmpty !"
  | TCEqual(tau1, tau2) -> TCEqual(sigma tau1, sigma tau2)
  | TCEqual_type(tau1,tau2) -> TCEqual_type(sigma tau1, sigma tau2)) c

let rec occurrence alpha tau =
  match tau with
  | Type_base _ -> false
  | Type_var (beta,_) -> alpha = beta
  (*| Type_scheme beta -> alpha = beta*)
  | Type_arrow ((tau1,_, tau2),_) -> occurrence alpha tau1 || occurrence alpha tau2
  | Type_pair ((tau1, tau2),_) -> occurrence alpha tau1 || occurrence alpha tau2
  | Type_par (tau,_) -> occurrence alpha tau
  | Type_tree (tau,_) -> occurrence alpha tau

let identite = fun typ -> typ 

let rec mgu c =
  if !flag_debug then ignore(List.map (
    function | TCEmpty -> failwith "TCEmpty"|TCEqual(a,b)|TCEqual_type(a,b) ->
						     Printf.printf "(%s,%s)\n" (fragment_to_string a)(fragment_to_string b)) c);
  match c with
    [] -> debug "identite";
      identite
  | TCEqual({tau=Type_var (alpha,_);fexp_loc=_;_},{tau=Type_var (beta,_);fexp_loc=_;_}) :: c' when alpha = beta -> debug "α==β";
    mgu c'
  | TCEqual_type({tau=Type_var (alpha,_);fexp_loc=_;_},{tau=Type_var (beta,_);fexp_loc=_;_}) :: c' when alpha = beta -> debug "α==β (type only)";
                                                                                                                        mgu c'
  | TCEqual({tau=Type_var (alpha,alpha_loc);effect=_;fexp_loc=_} as tau0, tau1) :: c' when not(occurrence alpha tau1.tau) ->
     debug "α,τ";
     (* Printf.printf "%i\n" (List.length tau1.e_cstr); *)
     (* ignore(List.map (fun x -> Printf.printf "///////////////////////////%s\n" (e_constraints_to_string x)) tau1.e_cstr); *)
     ignore(List.map (fun x -> add_to_gen_loc x) tau1.e_cstr);
     ignore(List.map (fun x -> add_to_gen_loc x) tau0.e_cstr);
     let gen = ((loc_of_exprtype tau0.tau),(loc_of_exprtype tau1.tau)) in
     (* Printf.printf "-*-*-*-equal1*-*-*-* %s\n" (e_constraints_to_string (ECEqual(build_chunk (fst gen) tau0.fexp_loc ,build_chunk (snd gen) tau1.fexp_loc))); *)
     add_to_gen_loc (ECEqual(build_chunk (fst gen) tau0.fexp_loc ,build_chunk (snd gen) tau1.fexp_loc));
     debug ("add loc cstr : "^(effect_to_string (fst gen))^"="^(effect_to_string (snd gen))^"\n");
     (*Tentative : subsituer le type mais pas la localité*)
     let new_tau1 = build_fragment (change_loc_of_exprtype tau1.tau alpha_loc) (tau1.effect) tau1.fexp_loc in
     let s = subst [alpha, new_tau1] in
    (*old (avant tentative)*)
    (*let s = subst [alpha, tau1] in *)
    let se = subst_equations s c' in
    (*effect_check tau0 tau1;*)
    let mgu_se = mgu se in
    compose mgu_se s
  | TCEqual_type({tau=Type_var (alpha,alpha_loc);fexp_loc=_;_} (*as tau0*), tau1) :: c' when not(occurrence alpha tau1.tau) -> debug "α,τ (type only)";
    (*Tentative : subsituer le type mais pas la localité*)
    let new_tau1 = build_fragment (change_loc_of_exprtype tau1.tau alpha_loc) (tau1.effect) tau1.fexp_loc in
    let s = subst [alpha, new_tau1] in
    (*old (avant tentative)*)
    (*let s = subst [alpha, tau1] in *)
    let se = subst_equations s c' in
    (*effect_check tau0 tau1;*)
    let mgu_se = mgu se in
    compose mgu_se s
  | TCEqual(tau0, {tau=Type_var (alpha,_) as tau1;fexp_loc=tau1_loc;_}) :: c' when not(occurrence alpha tau0.tau) ->
     debug "τ,α";
     let gen = ((loc_of_exprtype tau0.tau),(loc_of_exprtype tau1)) in
     (* Printf.printf "-*-*-*-*equal-*-*-* %s\n" (e_constraints_to_string (ECEqual(build_chunk (fst gen) tau0.fexp_loc,build_chunk (snd gen) tau1_loc))); *)
     add_to_gen_loc (ECEqual(build_chunk (fst gen) tau0.fexp_loc,build_chunk (snd gen) tau1_loc));
     debug ("add loc cstr : "^(effect_to_string (fst gen))^"="^(effect_to_string (snd gen))^"\n");
     let s = subst [alpha, tau0] in 
     let se = subst_equations s c' in
     let mgu_se = mgu se in
     let res = compose mgu_se s in 
     res
  | TCEqual({tau=Type_base (b1,_);fexp_loc=_;_}, {tau=Type_base (b2,_);fexp_loc=_;_}) :: c' when b1 = b2 -> debug "b==b";
                                                                                                            mgu c'
  | TCEqual_type({tau=Type_base (b1,_);fexp_loc=_;_}, {tau=Type_base (b2,_);fexp_loc=_;_}) :: c' when b1 = b2 -> debug "b==b";
    mgu c'
  (*Type of arrow need a check ? I think yeeeesss !*)
  | TCEqual(({tau=Type_arrow((tau1,l1, tau2),_);fexp_loc=_;_} as arr1),
	    ({tau=Type_arrow((tau1',l2,tau2'),_);fexp_loc=tau_loc;_} as arr2)) :: c' -> debug "->,->";
    debug (((effect_to_string (loc_of_exprtype tau2))^"="^(effect_to_string(loc_of_exprtype tau2')))^"\n");
    (*    let bf t = build_fragment t Dot tau_loc in)*)
    (*mgu((bf tau1, bf tau1') :: (bf tau2, bf tau2') :: c')*)
    (*Production des contraintes de localité*)
    let gen = (l1,l2) in
    (* Printf.printf "-*-*-*-Arrow*-*-*-* %s\n" (e_constraints_to_string (ECEqual((build_chunk (fst gen) arr1.fexp_loc,build_chunk (snd gen) arr2.fexp_loc)))); *)
    add_to_gen_loc (ECEqual(build_chunk (fst gen) arr1.fexp_loc,build_chunk (snd gen) arr2.fexp_loc));
    debug ((effect_to_string(loc_of_exprtype tau1))^"="^(effect_to_string(loc_of_exprtype tau1'))^"\n");
    (*let tau1'' = change_loc_of_exprtype tau1' (loc_of_exprtype tau1) in
      let tau2'' = change_loc_of_exprtype tau2' (loc_of_exprtype tau2) in*)
    mgu(
      TCEqual(build_fragment tau1 arr1.effect tau_loc, build_fragment tau1' arr2.effect tau_loc) :: 
	TCEqual(build_fragment tau2 arr1.effect tau_loc, build_fragment tau2' arr2.effect tau_loc) :: 
	c'
    )
  (*| TCEqual({tau=Type_pair(tau1, tau2);fexp_loc=_}, 
    {tau=Type_pair(tau1', tau2');fexp_loc=_}) :: c' -> debug "(,),(,)";
    failwith "TODO : MGU virer empty_fragment"
  (*mgu((empty_fragment tau1, empty_fragment tau1') :: (empty_fragment tau2, empty_fragment tau2') :: c')*)
  *)
  (*Type par test*)
  | TCEqual(tau,{tau=Type_par( (Type_var (alpha,_)),_);fexp_loc=_;_}) :: c' when not(occurrence alpha tau.tau) -> 
     debug "τ,α par";
    let s = subst [alpha, tau] in 
    let se = subst_equations s c' in
    let mgu_se = mgu se in
    compose mgu_se s
  | TCEqual(({tau=Type_par(alpha,_);fexp_loc=_;_} as tau1),({tau=Type_par(beta,_);fexp_loc=tloc;_} as tau2)) :: c' -> 
     debug "τ par,α par";
    mgu (TCEqual(build_fragment alpha (loc_of_exprtype alpha) tloc,build_fragment beta (loc_of_exprtype beta) tloc)::c')

(*
    (*Type par*)
  | (tau,{tau=Type_par( (Type_var (alpha,_)),_);cstr=_;loc=_;fexp_loc=_}) :: c' when not(occurrence alpha tau.tau) -> 
  debug "τ,α par";
  let s = subst [alpha, tau] in 
  let se = subst_equations s c' in
  let mgu_se = mgu se in
  compose mgu_se s
*)

(*| TCEqual_type(_) :: c -> failwith "MGU TODO : TCEqual_type"*)


(*Type tree*)
  | TCEqual(tau,{tau=Type_tree( (Type_var (alpha,_)),_);fexp_loc=_;_}) :: c' when not(occurrence alpha tau.tau) -> 
     debug "τ,α tree";
    let s = subst [alpha, tau] in 
    let se = subst_equations s c' in
    let mgu_se = mgu se in
    compose mgu_se s
  | TCEqual(({tau=Type_tree(alpha,_);fexp_loc=_;_}as tau1),({tau=Type_tree(beta,_);fexp_loc=tloc;_} as tau2)) :: c' -> 
     debug "τ tree,α tree";
    mgu (TCEqual(build_fragment alpha tau1.effect tloc,build_fragment beta tau2.effect tloc)::c')
      
  | c -> 
     let currcstr = (function | TCEmpty -> failwith "TCEmpty"| TCEqual(a,b)|TCEqual_type(a,b) -> (a,b))(List.hd c) in
     let given = (snd currcstr).tau in
     let expected = (fst currcstr).tau in
     raise (Wrong_Type (given,expected,(snd currcstr).fexp_loc))

