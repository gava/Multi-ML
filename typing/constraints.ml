open Types

(**Instance de type*)
type fragment =
  {
    tau : exprtype;
    effect : effect;
    e_cstr : e_constraints list;
    fexp_loc : Location.t;
  }

and chunk = 
  {
    loc : effect;
    cexp_loc : Location.t;
  }

and constraints =
| CEmpty
| CTrue
| CVar of fragment
| CType of fragment
| CConst of fragment
| COp of fragment
| CPair of constraints * constraints
| CSubtype of fragment * fragment
| CSubtype_type of fragment * fragment
| CEqual of fragment * fragment
| CEqual_type of fragment * fragment
| CExists of fragment * constraints
| CForall of fragment * constraints
| CAnd of constraints * constraints
| CDef of fragment * constraints * constraints
| CLet of fragment * constraints * constraints
| CTypescheme of constraints list * fragment
| CMax of chunk * chunk * chunk
| CAcceptloc_e of chunk * chunk
| CAcceptloc_t of fragment * chunk
| CAcceptdef_e of chunk * chunk
| CEqual_e of chunk * chunk
| CSeria of exprtype
(*Multi-ML*)

and e_constraints =
| ECEmpty
(*| ECAccept of effect * effect
| ECAccept_t of exprtype * effect*)
| ECTest
| ECAcceptloc of chunk * chunk
| ECAcceptloc_t of exprtype * chunk
| ECAcceptdef of chunk * chunk
| ECMax of chunk * chunk * chunk
| ECEqual of chunk * chunk
| ECSeria of exprtype

and t_constraints =
| TCEmpty
| TCEqual of fragment * fragment
| TCEqual_type of fragment * fragment

let fragment_to_string f =
  exprtype_to_string f.tau

let chunk_to_string f =
  effect_to_string f.loc


let e_constraints_to_string cstr =
  match cstr with
  | ECEmpty -> "[]"
  (*| ECAccept (e1,e2) -> (effect_to_string e1)^" ◁ "^(effect_to_string e2)
  | ECAccept_t (t,e) -> (exprtype_to_string t)^" ◁ "^(effect_to_string e)*)
  | ECTest -> "Test"
  | ECMax(eq,e1,e2) -> (chunk_to_string eq)^" = Max("^(chunk_to_string e1)^","^(chunk_to_string e2)^")"
  | ECAcceptloc(e1,e2) -> (chunk_to_string e1)^" ◁ "^(chunk_to_string e2)
  | ECAcceptloc_t(e1,e2) -> (exprtype_to_string e1)^" ◁ "^(chunk_to_string e2)
  | ECAcceptdef(e1,e2) -> (chunk_to_string e1)^" ◀ "^(chunk_to_string e2) 
  | ECEqual(e1,e2) -> (chunk_to_string e1)^" = "^(chunk_to_string e2)
  | ECSeria(e) -> "Seria("^(exprtype_to_string e)^")"

let rec constraints_to_string cstr =
  match cstr with
  | CEmpty -> ""
  | CTrue -> "true"
  | CVar v -> (*failwith "CVar is not acceptable"*) fragment_to_string v
  | CType _ -> failwith "CType"
  | CConst _ -> failwith "CConst"
  | COp op -> fragment_to_string op
  | CPair (p1,p2)-> "("^(constraints_to_string p1)^","^(constraints_to_string p2)^")"
  | CSubtype (e1,e2) -> (fragment_to_string e1)^" ≤ "^(fragment_to_string e2)
  | CSubtype_type (e1,e2) -> (fragment_to_string e1)^" ≤t "^(fragment_to_string e2)
  | CEqual (e1,e2) ->  (fragment_to_string e1)^" = "^(fragment_to_string e2)
  | CEqual_type (e1,e2) ->  (fragment_to_string e1)^" =t= "^(fragment_to_string e2)
  | CExists (e,c) -> "∃"^(fragment_to_string e)^"("^(constraints_to_string c)^")"
  | CForall (e,c) -> "∀"^(fragment_to_string e)^"("^(constraints_to_string c)^")."^(fragment_to_string e)
  | CDef (e1,e2,c) -> "def "^(fragment_to_string e1)^" : "^(constraints_to_string e2)^" in "^(constraints_to_string c)
  | CAnd (c1,c2) -> (constraints_to_string c1)^" ∧ "^(constraints_to_string c2)
  | CLet (e,c1,c2) -> "let "^(fragment_to_string e)^" : "^(constraints_to_string c1)^" in "^(constraints_to_string c2)
  | CTypescheme (c,e) -> 
    "⦇"^(List.fold_right (fun a b ->(constraints_to_string a)^"^"^b) c "")^"⦈."^(fragment_to_string e)
  | CMax(eq,e1,e2) -> (chunk_to_string eq)^" = Max("^(chunk_to_string e1)^","^(chunk_to_string e2)^")"
  | CAcceptloc_e(e1,e2) -> (chunk_to_string e1)^" ◁ "^(chunk_to_string e2)
  | CAcceptloc_t(e1,e2) -> (fragment_to_string e1)^" ◁ "^(chunk_to_string e2)
  | CAcceptdef_e(e1,e2) -> (chunk_to_string e1)^" ◀ "^(chunk_to_string e2) 
  | CEqual_e(e1,e2) -> (chunk_to_string e1)^" = "^(chunk_to_string e2)
  | CSeria e -> "Seria("^exprtype_to_string e^")"
  | _ -> failwith "TODO constraints_to_string : handle all constraints"

let cpt_levels = ref 0 
  
let new_level() = 
  incr cpt_levels; 
  "λ" ^ string_of_int !cpt_levels 

let cpt_vars = ref 0

let new_var() =
  incr cpt_vars;
  "α" ^ string_of_int !cpt_vars


(*let empty_fragment t =
  {
    tau = t;
    cstr = ECEmpty;
    loc = get_locality ();
    pexp_loc = Tools.fake_location;
  }
*)

let build_fragment t e ploc =
  {
    tau = t;
    effect = e;
    e_cstr = [];
    fexp_loc = ploc;
  }

let build_chunk e ploc =
  {
    loc = e;
    cexp_loc = ploc;
  }


(*Avoid unreadable CAnd*)
let rec build_cand =
  function 
  | f::s::[] -> CAnd(f,s)
  | f::s::tl -> CAnd(f,build_cand ([s]@tl))
  | [] -> failwith "Build CAnd : 2 arguments at least"
  | _::[] -> failwith "Build CAnd : 2 arguments at least"


let rec generate_constraints expr t cloc =
  match expr.expr with
  | Var v -> let delta = Level (new_level()) in let final = CSubtype(build_fragment (Type_var (v,delta)) Dot expr.p_loc,t) in (final,delta)(*cloc???*)
  (*| Var v -> let delta = Level (new_level()) in *)
  (* let c_var = CSubtype(build_fragment (Type_var (v,delta)) Dot expr.p_loc,t) in *)
  (* let c_loc = CAcceptloc_e(build_chunk delta expr.p_loc,build_chunk cloc expr.p_loc) in *)
  (* let final = CAnd(c_var,c_loc) in (final,delta)(\*cloc???*\) *)
  | Const _ -> let final = CSubtype(build_fragment (Type_base ("int",cloc)) Dot expr.p_loc,t) in (final,cloc)
  | Bool _ -> let final = CSubtype (build_fragment (Type_base ("bool",cloc)) Dot expr.p_loc,t) in (final,cloc)
  | Unit -> let final = CSubtype (build_fragment (Type_base ("unit",cloc)) Dot expr.p_loc,t) in (final,Dot)											       
  | String _ -> let final = CSubtype(build_fragment (Type_base ("string",cloc)) Dot expr.p_loc,t) in (final,cloc)
  | Op _ -> failwith "generate_constraints : op is useless ?"(* let final = COp(build_fragment (Type_arrow((Type_pair((Type_base ("int",get_locality()),Type_base ("int",get_locality())),get_locality()),get_locality (),Type_base ("int",get_locality())),get_locality())) ECEmpty (get_locality()) expr.p_loc) in (final,get_locality())*)
  

  | Pair (e1,e2) -> 
    let tau1 = Type_var (new_var (),cloc) in
    let tau1_frag = build_fragment tau1 Dot expr.p_loc in

    let tau2 = Type_var (new_var (),cloc) in
    let tau2_frag = build_fragment tau2 Dot expr.p_loc in

    let e1_cstr, e1_eff = generate_constraints e1 tau1_frag cloc in
    let e2_cstr, e2_eff = generate_constraints e2 tau2_frag cloc in 
    
    (*e1_eff == e2_eff*)
    if e1_eff <> e2_eff then failwith "pair : different effect -> to handle";

    let p_frag = build_fragment (Type_pair((tau1,tau2),cloc)) e1_eff expr.p_loc in
    let conj = CAnd(CAnd(e1_cstr,e2_cstr),CSubtype(p_frag,t)) in
    let final = CExists(tau1_frag,CExists(tau2_frag,conj)) in
    (final,cloc)

(*
    let alpha1 = Type_var (new_var (),eff) in
    let alpha1_frag = build_fragment alpha1 ECEmpty (get_locality()) expr.p_loc in
    let alpha2 = Type_var (new_var (),eff) in
    let alpha2_frag = build_fragment alpha2 ECEmpty (get_locality()) expr.p_loc in
    let c1,c1_ef = generate_constraints (p1,alpha1_frag,eff) in
    let c2,c2_ef = generate_constraints (p2,alpha2_frag,eff) in
    let tau = t in
    let cpair = (build_fragment (Type_pair(alpha1,alpha2)) ECEmpty (get_locality()) expr.p_loc, tau) in
    let cstrs = CAnd(CAnd(c1,c2),CSubtype(fst cpair,snd cpair)) in
    let final = CExists(alpha1_frag,CExists(alpha2_frag,cstrs)) in 
    (final,get_locality())
*)


  | Ifthenelse (c,e1,e2) -> 
    
    let cond = build_fragment (Type_base("bool",cloc)) Dot expr.p_loc in

    let c_cstr, c_eff = generate_constraints c cond cloc in

    (*let pi_a = Level (new_level ()) in 
    let alpha = Type_var(new_var (),pi_a) in
    let alpha_frag = build_fragment alpha pi_a e1.p_loc in *)
    let e1_cstr, e1_eff = generate_constraints e1 t cloc in

    (*let pi_b = Level (new_level ()) in
    let beta = Type_var(new_var(),pi_b) in
    let beta_frag = build_fragment beta pi_b e2.p_loc in*)
    let e2_cstr, _ = generate_constraints e2 t cloc in

    (*let eq_type = CEqual(alpha_frag,beta_frag) in*)

    let psi = Level (new_level ()) in
    let psi_cstr = CMax(build_chunk psi expr.p_loc, build_chunk c_eff c.p_loc, build_chunk e1_eff e1.p_loc) in
    
    let final = build_cand [c_cstr;e1_cstr;e2_cstr;(*eq_type;*)psi_cstr] in

    (final,cloc)

  | Fun (s,e) -> 
    let pi = Level (new_level ()) in
    let tau1 = Type_var (new_var (),pi) in
    let tau1_frag = build_fragment tau1 Dot expr.p_loc in

    let pi' = Level (new_level ()) in
    let tau2 = Type_var (new_var (),pi') in
    let tau2_frag = build_fragment tau2 Dot expr.p_loc in

    let new_loc = Level (new_level()) in
    let e_cstr, e_eff = generate_constraints e tau2_frag new_loc in

    (*update tau2_frag*)
    let tau2_frag = build_fragment tau1 e_eff expr.p_loc in

    let arrow = (CEqual(t,
			build_fragment (Type_arrow((tau1,e_eff,tau2),cloc)) e_eff expr.p_loc)) in
    let def = CLet(build_fragment (Type_var (s,pi)) Dot expr.p_loc,CVar(tau1_frag),e_cstr) in

    let acc_def = CAcceptdef_e(build_chunk e_eff e.p_loc,build_chunk cloc expr.p_loc) in

    let final = CAnd(
      CExists(tau1_frag,CExists(tau2_frag,CAnd(def,arrow))),
	      acc_def) in
    (final,e_eff)


  | App (e1,e2) ->
    let pi = Level (new_level ()) in
    let tau1 = Type_var (new_var(),pi) in
    (*Not needed anymore*)
    (*let tau1_frag = build_fragment tau1 Dot expr.p_loc in*)

    let pi' =(* Level (new_level ()) *) cloc in 
    let tau2 = Type_var (new_var(),pi') in 
    (*Not needed anymore*)
    (* let tau2_frag = build_fragment tau2 Dot expr.p_loc in *)

    let epsilon = Level (new_level()) in
    let e1_cstr, e1_eff = generate_constraints e1 (build_fragment (Type_arrow((tau1,epsilon,tau2),cloc)) epsilon expr.p_loc) cloc in

    (*update tau1_frag*)
    let tau1_frag = build_fragment tau1 e1_eff expr.p_loc in

    let pi'' = Level (new_level ()) in
    let tau1_pi''_frag = build_fragment (Type_var(type_of_exprtype tau1,pi'')) e1_eff expr.p_loc in
    let e2_cstr, e2_eff = generate_constraints e2 tau1_pi''_frag cloc in

    (*Locality propagation*)
    (*rewrite psi=max(e,e',e'') to psi=Max(e,psi') and psi'=Max(e',e'')*)
    let psi = Level (new_level ()) in
    let psi' = Level (new_level ()) in
    let psi_cstr = CMax(build_chunk psi expr.p_loc,build_chunk e1_eff e1.p_loc,build_chunk psi' e2.p_loc) in
    let psi'_cstr = CMax(build_chunk psi' expr.p_loc,build_chunk e2_eff e2.p_loc,build_chunk epsilon e1.p_loc) in
    
    (*update tau2_frag*)
    (*Not needed anymore*)
    (*let tau2_frag = build_fragment tau2 e2_eff expr.p_loc in*)

    let c_eq = CEqual(build_fragment (change_loc_of_exprtype tau2 cloc) psi expr.p_loc,t) in
    let acc_arg = CAcceptloc_e (build_chunk pi'' e2.p_loc,build_chunk pi expr.p_loc) in
    let acc_def = CAcceptdef_e (build_chunk epsilon e1.p_loc,build_chunk cloc expr.p_loc) in
    let acc_loc_def = CAcceptloc_e (build_chunk epsilon e1.p_loc,build_chunk cloc expr.p_loc) in
    let c_test = CAcceptloc_e (build_chunk pi'' e2.p_loc,build_chunk cloc expr.p_loc) in

    let conj = build_cand [c_eq;e1_cstr;e2_cstr;acc_arg;acc_loc_def;acc_def;psi_cstr;psi'_cstr;c_test] in

    let final = CExists(tau1_frag,conj) in

    (final,psi)

  | Let (x,e1,e2) ->
    let pi = Level (new_level ()) in
    let tau1 = Type_var (new_var(),pi) in
    let tau1_frag = build_fragment tau1 Dot expr.p_loc in
    let e1_cstr, e1_eff = generate_constraints e1 tau1_frag cloc in

    (*Update tau1_frag*)
    let tau1_frag = build_fragment tau1 e1_eff expr.p_loc in
    
    (*Not needed anymore ?*)
    (*let pi' = Level (new_level ()) in*)
    (*let tau2 = Type_var (new_var(),pi) in*)
    (* let tau2_frag = build_fragment tau2 Dot expr.p_loc in *)
    let e2_cstr, e2_eff = generate_constraints e2 t cloc in

    (*Update tau2_frag*)
    (*Not needed anymore ?*)
    (* let tau2_frag = build_fragment tau2 e2_eff expr.p_loc in *)

    (*let c_eq = CEqual(build_fragment (change_loc_of_exprtype tau2 cloc) ECEmpty cloc expr.p_loc,t) in*)
    (*Printf.printf "=====>> %s\n" (constraints_to_string c_eq) ;*)
    let type_scheme = CTypescheme ([CForall(tau1_frag,e1_cstr)],tau1_frag) in
    let phi = Level (new_level()) in
    let phi_cstr = CMax(build_chunk phi expr.p_loc,build_chunk e1_eff e1.p_loc, build_chunk e2_eff e2.p_loc) in
    let final = CAnd(
      CLet(build_fragment (Type_var(x,cloc)) e1_eff expr.p_loc,type_scheme,e2_cstr),
      (*CAnd(c_eq,*)phi_cstr)(* ) *) in    
    (final,phi)

  | Letrec(f,x,e1,e2) -> 
    let pi = Level (new_level ()) in
    let alpha = Type_var (new_var (),pi) in
    (*Not needed anymore ??*)
    (*let alpha_frag = build_fragment alpha Dot expr.p_loc in*)
    
    let pi' = Level (new_level ()) in
    let beta = Type_var (new_var (),pi') in
    let beta_frag = build_fragment beta Dot expr.p_loc in

    (*New location for the recursive call*)
    let new_loc = Level (new_level()) in
    let e1_cstr, e1_eff = generate_constraints e1 beta_frag new_loc in
    let e2_cstr, e2_eff = generate_constraints e2 t cloc in

    (*Update alpha_frag beta_frag*)
    let alpha_frag = build_fragment alpha e1_eff expr.p_loc in
    (*Not needed anymore ??*)
    (*let beta_frag = build_fragment beta e2_eff expr.p_loc in*)

    let tarrow = Type_arrow ((alpha,e1_eff,beta),cloc) in

    let letTS_x = CVar(build_fragment alpha e1_eff expr.p_loc) in
    let letTS_f = CVar(build_fragment tarrow e1_eff expr.p_loc) in
    let clet_x = CLet(build_fragment (Type_var (x,cloc)) e1_eff expr.p_loc,letTS_x,e1_cstr) in
    let clet_arrow = CLet(build_fragment (Type_var(f,cloc)) e1_eff expr.p_loc,letTS_f,clet_x) in

    let phi = Level (new_level()) in
    let phi_cstr = CMax(build_chunk phi expr.p_loc,build_chunk e1_eff e1.p_loc,build_chunk e2_eff e2.p_loc) in

    let final = CAnd(
      CLet(build_fragment (Type_var(f,cloc)) e1_eff expr.p_loc,CTypescheme ([CForall(alpha_frag,clet_arrow)],build_fragment tarrow e1_eff expr.p_loc),e2_cstr),
      phi_cstr) in
    (final,phi)
      

  | Replicate e -> 
    let y = new_level () in 
    let pi = Level (y) in
    let tau = Type_var(new_var (),pi) in
    let tau_frag = build_fragment tau Bsp expr.p_loc in
    let e_cstr, e_eff = generate_constraints e tau_frag Comm in

    let acc_eff = CAcceptloc_e(build_chunk e_eff e.p_loc,build_chunk Comm expr.p_loc) in
    let acc_tau = CAcceptloc_t(tau_frag,build_chunk Comm expr.p_loc) in
    let acc_pi = CAcceptloc_e(build_chunk pi e.p_loc,build_chunk Comm expr.p_loc) in
    let eq_loc = CEqual_e(build_chunk cloc expr.p_loc,build_chunk Bsp expr.p_loc) in

    let out_par = CEqual(build_fragment (Type_par (tau_frag.tau,Bsp)) Bsp expr.p_loc,t) in

    let cstrs = build_cand [acc_eff;acc_tau;acc_pi;eq_loc] in

    (*What that ? We need it ?*)
    (*let cstr_loc_in_par = CEqual_e(build_chunk pi expr.p_loc,build_chunk Comm expr.p_loc) in*)

    let final = CAnd(CAnd(e_cstr,out_par),cstrs) in
    (* failwith (constraints_to_string final); *)
    (final,Bsp)
     

  | Proj e -> 

     let pi = Level (new_level ()) in
     let alpha = new_var () in
     let alpha_frag = build_fragment (Type_par(Type_var(alpha,pi),Bsp)) Bsp expr.p_loc in
     let e_cstr, _ = generate_constraints e alpha_frag Bsp in
     let cstr_in_vec = CSeria (Type_var(alpha,pi)) in
     let out = build_fragment (Type_arrow((Type_base("int",Dot),Dot,Type_var(alpha,Dot)),Dot)) Dot expr.p_loc in
     let final = build_cand [CEqual(out,t);e_cstr;cstr_in_vec] in
     (final,Bsp)

     
  | Open x -> 

    let alpha = new_var () in
    let alpha_frag = build_fragment (Type_par(Type_var(alpha,cloc),Bsp)) Local expr.p_loc in
    let x_frag = build_fragment (Type_var(x,Bsp)) Bsp expr.p_loc in
    let c_eq = CSubtype(x_frag,alpha_frag) in
    let out_dummy = CEqual((*build_fragment (Type_base("int",cloc)) ECEmpty cloc expr.p_loc*)build_fragment (Type_var(alpha,cloc)) Local expr.p_loc,t) in 

    let final = CAnd(c_eq,out_dummy) in
    (final,Local)


(*
    CSubtype(build_fragment (Type_var (x,cloc)) ECEmpty cloc expr.p_loc,t)
    let e_cstr, e_eff = generate_constraints 
*)
(*
    if get_locality () == Local then (
      let alpha = Type_var (new_var()) in
      let alpha_frag = build_fragment (alpha) (ECEmpty) Bsp expr.p_loc in
      let e_cstr = CSubtype(build_fragment (Type_var e) ECEmpty Bsp expr.p_loc,alpha_frag) in
      Printf.printf "Type of e : %s : %s\n" (fragment_to_string (build_fragment (Type_var e) ECEmpty Bsp expr.p_loc)) (fragment_to_string alpha_frag);
      let final = CAnd(e_cstr,CEqual(alpha_frag,t)) in
      final
    )
    else 
      raise (Bad_Locality (("This expression is not allowed in locality ["^(effect_to_string_full( get_locality ()))^"]"),(Tools.fake_location)))
*)


  | Copy x -> 
    
    (*let alpha = new_var () in
    let alpha_frag = build_fragment (Type_var(alpha,Bsp)) expr.p_loc in
    let x_frag = build_fragment (Type_var(x,Bsp)) expr.p_loc in
    let c_sub = CSubtype(x_frag,alpha_frag) in
    let delta = Level (new_level ()) in
    let out = CEqual(build_fragment (Type_var(alpha,delta)) expr.p_loc,t) in
    let final = CAnd(out,(CAnd(c_sub,CEqual_e(build_chunk delta expr.p_loc, build_chunk Comm expr.p_loc)))) in
    (* failwith (constraints_to_string final); *)
    *)

    let dummy = new_var () in
    let dummy_l = Level(new_level ()) in
    let dummy_frag = build_fragment (Type_var(dummy,Bsp)) Bsp expr.p_loc in
    
    let alpha = new_var () in
    let alpha_frag = build_fragment (Type_var(alpha,Comm)) Local expr.p_loc in
    let x_frag = build_fragment (Type_var(x,Bsp)) Dot expr.p_loc in
    let c_sub = CSubtype_type(x_frag,alpha_frag) in
    let out = CEqual(build_fragment (Type_var(alpha,Local)) Local expr.p_loc, t) in

    let dummy_eq = CSubtype(build_fragment (Type_var(x,dummy_l)) Dot expr.p_loc,dummy_frag) in
    let dummy_loc_cstr = CEqual_e(build_chunk dummy_l expr.p_loc,build_chunk Bsp expr.p_loc) in

    (*peut être utiliser le generate_constraints pour avoir l'effet et tester l'acceptabilité de l'effet en c ??*)
    let acc_type_in_vec = CAcceptloc_t(dummy_frag,build_chunk Comm expr.p_loc) in

    let final = CAnd(out,CAnd(c_sub,CAnd(dummy_eq,CAnd(dummy_loc_cstr,acc_type_in_vec)))) in
 (* failwith (constraints_to_string final);  *)
    (final,Local)
    
(*
    if get_locality () == Local then (
      let alpha = Type_var (new_var ()) in
      let alpha_frag = build_fragment alpha (ECAccept_t(alpha,Bsp)) Bsp expr.p_loc in
      Printf.printf "Effect_cstr = [%s]%s\n" (e_constraints_to_string alpha_frag.cstr) (exprtype_to_string t.tau);
      let e_cstr = CSubtype (build_fragment (Type_var e) ECEmpty Bsp expr.p_loc,alpha_frag) in 
      let final = CAnd(e_cstr,CEqual(alpha_frag,t)) in
      final
    )
    else 
      raise (Bad_Locality (("This expression is not allowed in locality ["^(effect_to_string_full( get_locality ()))^"]"),(Tools.fake_location)))
*)


  | Letmultifun (f,x,e1,e2) ->

    let e_node,e_leaf = (function | Wherebody(e1,e2) -> (e1,e2) | _ -> failwith "Multi function body error …") e1.expr in
    
    (*Node*)
    let a = new_var () in
    let alpha = Type_var(a,Bsp) in
    let alpha_frag = build_fragment alpha Bsp expr.p_loc in

    (*no matter node_eff, it is Bsp. Right ?*)
    let node_cstr, _ = generate_constraints e_node alpha_frag Bsp in

     (*Leaf*)
    let b = new_var ()  in
    let beta = Type_var(b,Seq) in
    let beta_frag = build_fragment beta Seq expr.p_loc in
    
    (*no matter leaf_eff, it is seq. Right ?*)
    let leaf_cstr, _ = generate_constraints e_leaf beta_frag Seq in
    
    let tau = Type_var (new_var (),Multi) in
    let tau_frag = build_fragment tau Multi expr.p_loc in
    let tau_cstr = CEqual_type(tau_frag,alpha_frag) in


    (*Check node type == leaf type*)
    let eq_nl = CEqual_type(alpha_frag,beta_frag) in

    (*build multi function*)
    let f_frag = build_fragment (Type_var(f,Multi)) Multi expr.p_loc in
    let f_m = Type_arrow((Type_var(x,Multi),Multi,tau),Multi) in
    let f_type = CEqual(f_frag,build_fragment f_m Multi expr.p_loc) in

    (*build multi recursive function*)
    let tau_l = Type_var (new_var(),Comm) in
    let tau_l_frag = build_fragment tau_l Comm expr.p_loc in
    let tau_l_cstr = CEqual_type(tau_l_frag,alpha_frag) in
    let frec_frag = build_fragment (Type_var(f,Local)) Local expr.p_loc in
    let frec_l = Type_arrow((Type_var(x,Comm),Local,tau_l),Local) in
    (*Not needed anymore ?*)
    (*let frec_type = CEqual(frec_frag,build_fragment frec_l Local expr.p_loc) in*)
    
    (*Useless ?*)
    (*x_type*)
    (* let delta = Level(new_level ()) in *)
    (* let x_type = Type_var(new_var(),delta) in *)
    (*let x_cstr = CSubtype(build_fragment x_type delta expr.p_loc, build_fragment (Type_var(x,Multi)) Multi expr.p_loc) in*)

    (*x in env*)
    (*x in node*)
    
    let tau_x_node = Type_var(x,Bsp) in
    let letTS_x_node = CVar(build_fragment tau_x_node Bsp expr.p_loc) in
    let let_x_node = CLet(build_fragment (Type_var (x,Bsp)) Bsp expr.p_loc,letTS_x_node,node_cstr) in
    let let_x_node = CLet(frec_frag,CVar(build_fragment frec_l Local expr.p_loc),let_x_node) in

    (*x in leaf*)
    let tau_x_leaf = Type_var(x,Seq) in
    let letTS_x_leaf = CVar(build_fragment tau_x_leaf Seq expr.p_loc) in
    let let_x_leaf = CLet(build_fragment (Type_var (x,Seq)) Seq expr.p_loc,letTS_x_leaf,leaf_cstr) in


    (*no matter e2_eff, it is Multi. Right ?*)
    let e2_cstr, _ = generate_constraints e2 t cloc in

    let final = build_cand [tau_l_cstr;(*frec_type;*)let_x_node;let_x_leaf;tau_cstr;eq_nl;f_type;e2_cstr] in

  
    (final,Multi)

(*
    (*Node type*)
    let pi = Level (new_level()) in
    let beta = new_var () in
    let beta_node = Type_var (beta,pi) in
    let beta_node_frag = build_fragment beta_node Bsp expr.p_loc in

    (*Leaf type*)
    let pi' = Level (new_level()) in
    let beta_leaf = Type_var (new_var (),pi') in
    let beta_leaf_frag = build_fragment beta_leaf Seq expr.p_loc in

    (*Multi recursive function*)

    let e_node_cstr, e_node_eff = generate_constraints e_node beta_node_frag Bsp in
    let e_leaf_cstr, e_leaf_eff = generate_constraints e_leaf beta_leaf_frag Seq in 

    (*x type*)
    let pi'' = Level (new_level()) in
    let alpha_var = new_var () in
    let alpha = Type_var (alpha_var,pi'') in
    let alpha_frag = build_fragment alpha Dot expr.p_loc in

    let letTS_x = CVar(build_fragment alpha Dot expr.p_loc) in
    let clet_x = CLet(build_fragment (Type_var (x,cloc)) Dot expr.p_loc,letTS_x,e_node_cstr) in

    let alpha_in = Type_var(alpha_var,Multi) in
    let delta = Type_var(new_var(),Multi) in
    let tarrow_out = Type_arrow((alpha_in,Multi,delta),Multi) in

    let alpha_in = Type_var(alpha_var,Comm) in
    let beta_out = Type_var(beta,Comm) in
    let tarrow_rec = Type_arrow((alpha_in,Local,beta_out),Local) in
    let letTS_f = CVar(build_fragment tarrow_rec Dot expr.p_loc) in

    let clet_node = CLet(build_fragment (Type_var (f,cloc)) Dot expr.p_loc,letTS_f,clet_x) in
    let clet_leaf = CLet(build_fragment (Type_var (x,cloc)) Dot expr.p_loc,letTS_x,e_leaf_cstr) in

    let e2_cstr, e2_eff = generate_constraints e2 t cloc in

    let c_and = CAnd(clet_node,
		     CAnd(clet_leaf,
			  CAnd(
			    CEqual(beta_node_frag,beta_leaf_frag),
			    CEqual(build_fragment delta Dot expr.p_loc,beta_node_frag))
		     )
    ) in
    let final = CLet(build_fragment (Type_var (f,cloc)) Dot expr.p_loc,
		     CTypescheme ( CForall(alpha_frag,c_and),
				   build_fragment tarrow_out Dot expr.p_loc),
		     e2_cstr) in
    (* Printf.printf "Final multi : %s" (constraints_to_string final); *)
    (final,Multi)
*)

(*
    let pi = Level (new_level()) in
    let alpha = Type_var (new_var (),pi) in
    let alpha_frag = build_fragment alpha ECEmpty pi expr.p_loc in

    let pi' = Level (new_level()) in
    let beta = Type_var (new_var (),pi') in
    let beta_frag = build_fragment beta ECEmpty Bsp expr.p_loc in

    let delta = Type_var (new_var ()) in
    let delta_frag = build_fragment delta ECEmpty Seq expr.p_loc in

    (*let beta_frag = empty_fragment beta in*)
    let tarrow = Type_arrow ((alpha,Multi,),Multi) in

    let node_cstr, node_eff = generate_constraints e_node beta_frag Bsp in
    let leaf_cstr, leaf_eff = generate_constraints e_leaf delta_frag Seq in

    Printf.printf "beta : %s\n" (exprtype_to_string beta);
    Printf.printf ("c_node : %s\n") (constraints_to_string c_node);
    Printf.printf ("c_leaf : %s\n") (constraints_to_string c_leaf);
    let c2 = generate_constraints (e2,t,eff) in
    let letTS_x = CVar(build_fragment alpha ECEmpty (get_locality()) expr.p_loc) in
    let letTS_f = CVar(build_fragment tarrow ECEmpty (get_locality()) expr.p_loc) in
    let clet_x = CLet(build_fragment (Type_var x) ECEmpty (get_locality()) expr.p_loc,letTS_x,c_node) in
    let clet_node = CLet(build_fragment (Type_var f) ECEmpty (get_locality ()) expr.p_loc,letTS_f,clet_x) in
    let clet_leaf = CLet(build_fragment (Type_var x) ECEmpty (get_locality()) expr.p_loc,letTS_x,c_leaf) in
    (*On dit que beta = delta par pour valider le type node et leaf*)
    let c_and = CAnd(clet_node,CAnd(clet_leaf,CEqual(beta_frag,build_fragment delta ECEmpty Seq expr.p_loc ))) in
    let final = CLet(build_fragment (Type_var f) ECEmpty (get_locality()) expr.p_loc,CTypescheme (CForall(alpha_frag,c_and),build_fragment tarrow ECEmpty (get_locality()) expr.p_loc),c2) in
    Printf.printf "Final multi : %s" (constraints_to_string final);
    final
*)

  | Letmultitreefun (f,x,e1,e2) -> 
     let e_node,e_leaf = (function | Wherebody(e1,e2) -> (e1,e2) | _ -> failwith "Multi function body error …") e1.expr in
     
    (*Node*)
    let a = new_var () in
    let alpha = Type_var(a,Bsp) in
    let alpha_frag = build_fragment alpha Bsp expr.p_loc in

    (*no matter node_eff, it is Bsp. Right ?*)
    let node_cstr, _ = generate_constraints e_node alpha_frag Bsp in

     (*Leaf*)
    let b = new_var ()  in
    let beta = Type_var(b,Seq) in
    let beta_frag = build_fragment beta Seq expr.p_loc in
    
    (*no matter leaf_eff, it is seq. Right ?*)
    let leaf_cstr, _ = generate_constraints e_leaf beta_frag Seq in
    
    let tau = Type_var (new_var (),Multi) in
    let tau_frag = build_fragment tau Multi expr.p_loc in
    let tau_cstr = CEqual_type(tau_frag,alpha_frag) in


    (*Check node type == leaf type*)
    let eq_nl = CEqual_type(alpha_frag,beta_frag) in

    (*build multi function*)
    let f_frag = build_fragment (Type_var(f,Multi)) Multi expr.p_loc in
    let f_m = Type_arrow((Type_var(x,Multi),Multi,Type_tree(tau,Multi)),Multi) in
    let f_type = CEqual(f_frag,build_fragment f_m Multi expr.p_loc) in

    (*build multi recursive function*)
    let tau_l = Type_var (new_var(),Comm) in
    let tau_l_frag = build_fragment tau_l Comm expr.p_loc in
    let tau_l_cstr = CEqual_type(tau_l_frag,alpha_frag) in
    let frec_frag = build_fragment (Type_var(f,Local)) Local expr.p_loc in
    let frec_l = Type_arrow((Type_var(x,Comm),Local,Type_tree(tau_l,Local)),Local) in
    (*Not needed anymore ?*)
    (*let frec_type = CEqual(frec_frag,build_fragment frec_l Local expr.p_loc) in*)
    
    (*Useless ?*)
    (*x_type*)
    (* let delta = Level(new_level ()) in *)
    (* let x_type = Type_var(new_var(),delta) in *)
    (*let x_cstr = CSubtype(build_fragment x_type delta expr.p_loc, build_fragment (Type_var(x,Multi)) Multi expr.p_loc) in*)

    (*x in env*)
    (*x in node*)
    
    let tau_x_node = Type_var(x,Bsp) in
    let letTS_x_node = CVar(build_fragment tau_x_node Bsp expr.p_loc) in
    let let_x_node = CLet(build_fragment (Type_var (x,Bsp)) Bsp expr.p_loc,letTS_x_node,node_cstr) in
    let let_x_node = CLet(frec_frag,CVar(build_fragment frec_l Local expr.p_loc),let_x_node) in

    (*x in leaf*)
    let tau_x_leaf = Type_var(x,Seq) in
    let letTS_x_leaf = CVar(build_fragment tau_x_leaf Seq expr.p_loc) in
    let let_x_leaf = CLet(build_fragment (Type_var (x,Seq)) Seq expr.p_loc,letTS_x_leaf,leaf_cstr) in


    (*no matter e2_eff, it is Multi. Right ?*)
    let e2_cstr, _ = generate_constraints e2 t cloc in

    let final = build_cand [tau_l_cstr;(*frec_type;*)let_x_node;let_x_leaf;tau_cstr;eq_nl;f_type;e2_cstr] in

  
    (final,Multi)
      
  | Wherebody _ -> failwith "Impossible to use Wherebody outside a multi functions body !"

  | Mktree e -> 
    let pi = Level (new_level ()) in
    let alpha = Type_var (new_var (),pi) in
    let alpha_frag = build_fragment alpha Comm expr.p_loc in

    let e_cstr,e_eff = generate_constraints e alpha_frag Comm in

    let acc_eff = CAcceptloc_e(build_chunk e_eff e.p_loc,build_chunk Comm expr.p_loc) in
    let acc_tau = CAcceptloc_t(alpha_frag,build_chunk Comm expr.p_loc) in
    let acc_pi = CAcceptloc_e(build_chunk pi e.p_loc,build_chunk Comm expr.p_loc) in
    (*let eq_loc = CEqual_e(build_chunk cloc expr.p_loc,build_chunk Multi expr.p_loc) in*)

    let out_tree = CEqual(build_fragment (Type_tree (alpha_frag.tau,Multi)) Multi expr.p_loc,t) in

    let cstrs = build_cand [acc_eff;acc_tau;acc_pi(*;eq_loc*)] in

    let final = CAnd(CAnd(e_cstr,out_tree),cstrs) in
    (final,Multi)

  | At s ->
    let alpha = new_var () in
    (*Warning -> Type_var(alpha,Dot) --> Dot is Ok ? No pb ??*)
    let alpha_frag = build_fragment (Type_tree(Type_var(alpha,Dot),Multi)) Multi expr.p_loc in
    let s_frag = build_fragment (Type_var(s,Multi)) Multi expr.p_loc in
    let c_eq = CSubtype(s_frag,alpha_frag) in
    
    let out = CEqual(build_fragment (Type_var(alpha,cloc)) Multi expr.p_loc,t) in
    let final = CAnd(c_eq,out) in
    (final,cloc)



       
(*old->new ids*)
let env_original : (string,string) Hashtbl.t = Hashtbl.create 1000
let _ = Hashtbl.add env_original "(+)" "(+)";;

let index = ref 0 ;;
let free_index () = let r = "x"^(string_of_int !index) in incr index;r;;

(*Rename all vars in expression in order to make it free !*)
let make_it_free expr =
  let rec substitute expr old fresh =
    let res = 
      begin
	match expr.expr with
	| Var v when v = old -> Var fresh
	| Var v -> Var v
	| Const c -> Const c
	| Bool b -> Bool b
	| Unit -> Unit
	| String s -> String s
	| Op op -> Op op
	| Ifthenelse (c,e1,e2) -> Ifthenelse(substitute c old fresh,substitute e1 old fresh, substitute e2 old fresh)
	| Pair (p1,p2) -> Pair (substitute p1 old fresh,substitute p2 old fresh)
	| Fun (s,e) -> 
	  if s = old then
	    Fun (s,e)
	  else 
	    Fun (s,substitute e old fresh)
	| App (a1,a2) -> App (substitute a1 old fresh, substitute a2 old fresh) (*a2 :'a list dans l'ast*)
	| Let (x,e1,e2) -> 
	  if x = old then 
	    Let (x,e1,e2)
	  else
	    Let (x, substitute e1 old fresh, substitute e2 old fresh)
	| Letrec (f,x,e1,e2) -> 
	  if f = old || x = old  then 
	    Letrec (f,x,e1,e2)
	  else
	    Letrec (f,x, substitute e1 old fresh, substitute e2 old fresh)
	| Replicate e -> Replicate (substitute e old fresh)
	| Proj e -> Proj (substitute e old fresh)
	| Open s when s = old -> Open fresh
	| Open s -> Open s
	| Copy s when s = old -> Copy fresh
	| Copy s -> Copy s
	| Letmultifun (f,x,e1,e2) ->
	   if f = old || x = old then
	     Letmultifun (f,x,e1,e2)
	   else 
	     Letmultifun (f,x,substitute e1 old fresh,substitute e2 old fresh)
	| Letmultitreefun (f,x,e1,e2) ->
	   if f = old || x = old then
	     Letmultitreefun (f,x,e1,e2)
	   else 
	     Letmultitreefun (f,x,substitute e1 old fresh,substitute e2 old fresh)
	| Wherebody (e1,e2) ->
	   Wherebody (substitute e1 old fresh, substitute e2 old fresh)
	| Mktree e -> Mktree (substitute e old fresh)
	| At s when s = old -> At fresh
	| At s -> At s
      end
    in mk_expression res expr.p_loc
  in
  let rec make_it_free expr old fresh =
    let res = 
      begin
	match expr.expr with
	| Var v when v = old -> Var fresh
	| Var v -> Var v
	| Const c -> Const c
	| Bool b -> Bool b
	| Unit -> Unit
	| String s -> String s
	| Op op -> Op op 
	| Ifthenelse (c,e1,e2) -> 
	  Ifthenelse(make_it_free c "" "", make_it_free e1 "" "", make_it_free e2 "" "")
	| Pair (p1,p2) -> Pair (make_it_free p1 "" "",make_it_free p2 "" "")
	| Fun (x,e) -> 
	  let new_x = free_index () in
	  Hashtbl.add env_original new_x x;
	  let prop = substitute e x new_x in
	  Fun (new_x,make_it_free prop "" "")
	| App (a1,a2) ->
	  App(make_it_free a1 "" "",make_it_free a2 "" "")
	| Let (x,e1,e2) ->
	  let new_x = free_index () in
	  Hashtbl.add env_original new_x x;
	  let prop_e2 = substitute e2 x new_x in
	  Let (new_x,make_it_free e1 "" "",make_it_free prop_e2 "" "")
	| Letrec (f,x,e1,e2) ->
	  let new_f = free_index () in
	  let new_x = free_index () in
	  Hashtbl.add env_original new_f f;
	  Hashtbl.add env_original new_x x;
	  let prop_e1 = substitute e1 x new_x in
	  let prop_e2 = substitute e2 x new_x in
	  let prop_2_e1 = substitute prop_e1 f new_f in
	  let prop_2_e2 = substitute prop_e2 f new_f in
	  Letrec(new_f,new_x,make_it_free prop_2_e1 "" "", make_it_free prop_2_e2 "" "")
	| Replicate e -> Replicate (make_it_free e "" "")
	| Proj e -> Proj (make_it_free e "" "")
	| Open s -> Open s
	| Copy s -> Copy s
	| Letmultifun (f,x,e1,e2) -> 
	  let new_f = free_index () in
	  let new_x = free_index () in
	  Hashtbl.add env_original new_f f;
	  Hashtbl.add env_original new_x x;
	  let prop_e1 = substitute e1 f new_f in
	  let prop_1_e1 = substitute prop_e1 x new_x in
	  let prop_e2 = substitute e2 f new_f in
	  let prop_1_e2 = substitute prop_e2 x new_x in
	  Letmultifun (new_f,new_x,make_it_free prop_1_e1 "" "",make_it_free prop_1_e2 "" "")
	| Letmultitreefun (f,x,e1,e2) -> 
	  let new_f = free_index () in
	  let new_x = free_index () in
	  Hashtbl.add env_original new_f f;
	  Hashtbl.add env_original new_x x;
	  let prop_e1 = substitute e1 f new_f in
	  let prop_1_e1 = substitute prop_e1 x new_x in
	  let prop_e2 = substitute e2 f new_f in
	  let prop_1_e2 = substitute prop_e2 x new_x in
	  Letmultitreefun (new_f,new_x,make_it_free prop_1_e1 "" "",make_it_free prop_1_e2 "" "")
	| Wherebody (e1,e2) ->
	  Wherebody(make_it_free e1 "" "",make_it_free e2 "" "")
	| Mktree e -> Mktree (make_it_free e "" "")
	| At s -> At s
      end
    in mk_expression res expr.p_loc
  in
  make_it_free expr "" "" 


(*let rec substitute_cstr cstr old fresh =
  match cstr with
  | CVar old -> CVar fresh
  | CConst c -> CConst c
  | COp op -> COp op
  | CPair (p1,p2) -> CPair(substitute_cstr p1 old fresh, substitute_cstr p2 old fresh)
  | CSubtype (old,expr) -> CSubtype(fresh,expr) (*Reverse way ??*)
  | CExists (e,c) -> CExists (e,substitute_cstr c old fresh)
  | CForall (e,c) -> CForall (e,substitute_cstr c old fresh)
  | CDef (e1,e2,c) -> CDef(e1,e2,substitute_cstr c old fresh)
  | CAnd (c1,c2) -> CAnd (substitute_cstr c1 old fresh, substitute_cstr c2 old fresh)
  | CLet (e,c1,c2) -> CLet(e,substitute_cstr c1 old fresh, substitute_cstr c2 old fresh)
  | CTypescheme (c,_) -> substitute_cstr c old fresh
  | _ -> failwith "TODO substitute_cstr : handle all constraints" 
;;*)
    
(*
let rec link_it (expr : expression) =
  let resolve_var v = 
    try 
      Hashtbl.find env_original v 
    with
    | Not_found -> raise (Unbound_Value (v,expr.p_loc))
  in
  let res = 
    begin
      match expr.expr with
      | Var v -> 
	let org = resolve_var v in
	Var org
      | Const c -> Const c
      | String s -> String s
      | Op op -> Op op
      | Pair (p1,p2) -> Pair (link_it p1,link_it p2)
      | Fun (s,e) -> 
	let org = resolve_var s in
	Fun (org,link_it e)
      | App (a1,a2) -> App (link_it a1,link_it a2)
      | Let (x,e1,e2) -> 
	let org = resolve_var x in
	Let (org,link_it e1,link_it e2)
      | Letrec (f,x,e1,e2) -> 
	let orgf = resolve_var f in
	let orgx = resolve_var x in
	Letrec (orgf,orgx,link_it e1,link_it e2)
      | _ -> failwith "TODO link_it : handle all expressions" 
    end 
  in mk_expression res expr.p_loc
*)
