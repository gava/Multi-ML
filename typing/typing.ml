open Parsetree
open Tools
open Types
open Constraints
open Solver


let flag_debug = ref false
(* let debug s = if !flag_debug then Printf.printf "%s\n" s *)

let gen_loc = Type_unification.gen_loc;; 
    
let abcdize t =
  (*List of types*)
  let lst_t = ref [] in
  (*List of loc*)
  let lst_l = ref [] in
  (*Update the list of types*)
  let add_to_lst_t e = if not(List.exists (fun x -> e = x) !lst_t) then (lst_t := (!lst_t)@[e]) in
  (*Update the list of loc*)
  let add_to_lst_l e = if not(List.exists (fun x -> e = x) ((!lst_l)@[Seq;Local;Comm;Bsp;Multi;Dot])) then (lst_l := (!lst_l)@[e]) in
  let rec subst_t t' old fresh =
    begin
      match t' with
      | Type_base (b,eff) -> Type_base (b,eff)
      | Type_var (v,eff) when v = old -> Type_var (fresh,eff)
      | Type_var (v,eff) -> Type_var (v,eff)
      | Type_pair ((p1,p2),eff) ->
	Type_pair ((subst_t p1 old fresh, subst_t p2 old fresh),eff)
      | Type_arrow ((a1,ef,a2),eff) ->
	Type_arrow ((subst_t a1 old fresh,ef,subst_t a2 old fresh),eff)
      | Type_par (a,eff) -> Type_par ((subst_t a old fresh),eff)
      | Type_tree (a,eff) -> Type_tree ((subst_t a old fresh),eff)
    end in
  let rec subst_l l' old fresh =
    begin
      match l' with
      | Type_base (b,eff) when eff = old -> Type_base (b,fresh)
      | Type_base (b,eff) -> Type_base (b,eff)

      | Type_var (v,eff) when eff = old -> Type_var (v,fresh)
      | Type_var (v,eff) -> Type_var (v,eff)

      | Type_pair ((p1,p2),eff) when eff = old ->
	Type_pair ((subst_l p1 old fresh, subst_l p2 old fresh),fresh)
      | Type_pair ((p1,p2),eff) ->
	Type_pair ((subst_l p1 old fresh, subst_l p2 old fresh),eff)

      | Type_arrow ((a1,ef,a2),eff) ->
	let ef = if ef = old then fresh else ef in
	let eff = if eff = old then fresh else eff in
	Type_arrow ((subst_l a1 old fresh,ef,subst_l a2 old fresh),eff)

      | Type_par (a,eff) when eff = old -> Type_par ((subst_l a old fresh),fresh)
      | Type_par (a,eff) -> Type_par ((subst_l a old fresh),eff)

      | Type_tree (a,eff) when eff = old -> Type_tree((subst_l a old fresh),eff)
      | Type_tree (a,eff) -> Type_tree ((subst_l a old fresh),eff)
    end in
  (*Index of poly types*)
  let abc = ref 97 in
  let new_abc () = 
    let res = "'"^(Char.escaped (Obj.magic (!abc))) in 
    incr abc ; res in
  (*Index of poly loc*)
  let zyx = ref 122 in
  let new_zyx () = 
    let res = "`"^(Char.escaped (Obj.magic (!zyx))) in 
    decr zyx ; res in
  (*Go through tagged type and find type and loc*)
  let rec enum t =
    match t with
    | Type_base (_,e) -> add_to_lst_l e
    | Type_var (v,e) -> add_to_lst_t v; add_to_lst_l e
    | Type_pair ((p1,p2),e) -> enum p1;enum p2;add_to_lst_l e
    | Type_arrow ((a1,el,a2),e) -> enum a1;enum a2;add_to_lst_l el;add_to_lst_l e
    | Type_par (a,e) -> enum a;add_to_lst_l e
    | Type_tree (a,e) -> enum a;add_to_lst_l e
  in
  (*Build type assoc*)
  enum t ;
  let new_types = List.map (fun x -> (x,new_abc ())) !lst_t in
  let new_loc = List.map (fun x -> (x,Level (new_zyx()))) !lst_l in
  if !flag_debug then (
  ignore (List.map (fun x -> Printf.printf "%s\n%!" (effect_to_string x)) !lst_l); 
  ignore (List.map (fun x -> Printf.printf "%s\n%!" ( x)) !lst_t); 
  ignore (List.map (fun (x,y) -> Printf.printf "%s -> %s\n%!" x y) new_types); 
  ignore (List.map (fun (x,y) -> Printf.printf "%s -> %s\n%!" (effect_to_string x) (effect_to_string y)) new_loc);()); 
  let rec do_sub_t l t =
    begin
      match l with
      | [] -> t
      | hd::[] -> subst_t t (fst hd) (snd hd)
      | hd::tl -> 
	let new_t = subst_t t (fst hd) (snd hd) in
	do_sub_t tl new_t
    end
  in
  let rec do_sub_l lst l =
    begin
      match lst with
      | [] -> l
      | hd::[] -> subst_l l (fst hd) (snd hd)
      | hd::tl -> 
	let new_l = subst_l l (fst hd) (snd hd) in
	do_sub_l tl new_l
    end
  in
  let step1 = do_sub_t new_types t in
  let step2 = do_sub_l new_loc step1 in
  (step2,(new_types,new_loc))


let pexp_expr_to_expression_t (ast : Parsetree.structure ) =
  let rec process_exp e =
    let res = 
      begin
	match e.pexp_desc with
	| Pexp_ident id -> 
	  Var(lident_to_string (Asttypes.(id.txt)))
	| Pexp_construct (lid,_) -> 
	  begin 
	    match lident_to_string (Asttypes.(lid.txt)) with
	    | "true" -> Bool true
	    | "false" -> Bool false
	    | "()" -> Unit
	    | "::" -> failwith "list … TODO"
	    | "[]" -> failwith "list … TODO"
	    | _ -> failwith "Pexp_construct ? TODO : Pexp_construct *"
	  end
	| Pexp_constant c ->
	  begin
	    match c with
	    | Asttypes.Const_int _ -> Const 1
	    | Asttypes.Const_string _ -> String ""
	    | _ -> failwith "TODO : Pexp_constant *"
	  end
	| Pexp_tuple e -> 
	  Pair(process_exp (List.hd e),process_exp (List.hd (List.tl e)))
	| Pexp_ifthenelse (c,e1,e2) -> 
	  let e2 = (function | Some e -> e | None -> failwith "Ifthenelse : No else") e2 in
	  Ifthenelse(process_exp c,process_exp e1, process_exp e2)
        | Pexp_sequence (e1,e2) ->
           failwith "TODO : type Pexp_sequence …"
           (* Sequence(process_exp e1,process_exp e2) *)
	| Pexp_let (rf,vbl,e_in) -> 
	  begin
	    match rf with
	    | Asttypes.Nonrecursive ->
	      let e1 = List.hd vbl in
	      let f = e1.pvb_pat in
	      let e1 = e1.pvb_expr in
	      Let(ppat_var_to_str f,process_exp e1,process_exp e_in)
	    | Asttypes.Recursive -> 
	      let x,e1 = (function | Pexp_fun(_,_,p,e) -> p,e | _ -> failwith "pexp_expr_to_expression_desc : match fail") (List.hd vbl).pvb_expr.pexp_desc in
	      let f = (List.hd vbl).pvb_pat in
	      Letrec(ppat_var_to_str f,ppat_var_to_str x,process_exp e1,process_exp e_in)
	    | Asttypes.Multirecursive ->
	      let _,x,e = (function | Pexp_multi_function (p,[h,t]) -> (p,h,t) | _ -> failwith "Multirecursive function malformed") (List.hd vbl).pvb_expr.pexp_desc in 
	      let f = (List.hd vbl).pvb_pat in
	      let whereb = process_exp e in
	      Letmultifun (ppat_var_to_str f, ppat_var_to_str x, whereb ,process_exp e_in)
	    | Asttypes.Multitreerecursive ->
	       let _,x,e = (function | Pexp_multi_function (p,[h,t]) -> (p,h,t) | _ -> failwith "Multirecursive function malformed") (List.hd vbl).pvb_expr.pexp_desc in 
	       let f = (List.hd vbl).pvb_pat in
	       let whereb = process_exp e in
	       Letmultitreefun (ppat_var_to_str f, ppat_var_to_str x, whereb ,process_exp e_in)
	  end
	| Pexp_multi_function (_) ->
	  failwith "Todo : Multi fonction Pstr_value"
	| Pexp_wherebody (node,leaf) -> 
	  Wherebody (process_exp node,process_exp leaf)
	| Pexp_fun (_,_,p,e) ->
	  Fun(ppat_var_to_str p,process_exp e)
	| Pexp_apply (a1,a2) ->
	  let rec f x = match x with 
	    | hd::tl::[]  -> App(process_exp(hd),process_exp(tl)) 
	    (*?????*)
	    | hd::tl -> App(mk_expression (f (hd::(List.rev(List.tl (List.rev tl))))) e.Parsetree.pexp_loc ,process_exp (List.hd (List.rev tl)))
	    | [] -> failwith "f in process_exp : Empty list"
	  in
	  let a2' = List.map (fun (_,y) -> y) a2 in
	  f (a1::a2')
	| Pexp_replicate e -> 
	  Replicate (process_exp e)
	| Pexp_open_vector id ->
	  Open (lident_to_string (Asttypes.(id.txt)))
	| Pexp_copy id -> 
	  Copy (lident_to_string (Asttypes.(id.txt)))
	| Pexp_proj e -> Proj (process_exp e)
	| Pexp_put _ -> failwith "put is in env"
        | Pexp_mkpar e -> App(mk_expression (Var("mkpar")) e.Parsetree.pexp_loc,process_exp e) 
	| Pexp_mktree _ -> failwith "mktree is in env"(*Mktree (process_exp e)*)
	| Pexp_at s -> At(lident_to_string (Asttypes.(s.txt)))
	| _ -> failwith "TODO : pexp_expr_to_expression_desc"
      end 
    in mk_expression res e.Parsetree.pexp_loc
  in
  let process_pstr ast =
    try
      begin
	match ast.pstr_desc with
	| Pstr_value (l,vbl) ->
           if (function | Asttypes.Multirecursive -> true
                        | Asttypes.Multitreerecursive -> failwith ("Todo Multitreerecursive")
                        |_ -> false) l then
             Printf.printf "-->Multifun\n"
           ;
           process_exp ((List.hd vbl).pvb_expr)
	| Pstr_eval (e,_) -> process_exp e
	| _ -> failwith "Process_Pstr : TODO"
      end
    with 
    | Failure "hd" -> Printf.printf "No expression to type …\n"; exit 1
  in
  let res = List.map (fun x -> process_pstr x) ast in
  (*Printf.printf "[%s]\n" (expression_desc_to_string res);*) res
  


(**Associate locality from tagged type*)
let assoc_locality tau env_l =
  let assoc_pair_loc l (env : (Constraints.chunk * Constraints.chunk) list) =
    try
      (snd (List.find (fun (x,_) -> if l = x.loc then true else false) env)).loc
    with 
    | Not_found -> l
  in
  let rec do_it tau env_l =
    begin
      match tau with
      | Type_var (t,l) -> 
	let l' = assoc_pair_loc l env_l in
	Type_var (t,l')
      | Type_base (t,l) -> 
	let l' = assoc_pair_loc l env_l in
	Type_base (t,l')
      | Type_arrow ((left,lat_eff,right),eff) -> 
	let lat_eff' = assoc_pair_loc lat_eff env_l in 
	let eff' = assoc_pair_loc eff env_l in
	let left' = do_it left env_l  in
	let right' = do_it right env_l in
	Type_arrow ((left',lat_eff',right'),eff')
      | Type_pair ((p0,p1),l) ->
	let p0' = do_it p0 env_l in
	let p1' = do_it p1 env_l in
	let l = assoc_pair_loc l env_l in
	Type_pair ((p0',p1'),l)
      | Type_par (t,l) -> 
	let t' = do_it t env_l in
	let l' = assoc_pair_loc l env_l in
	Type_par (t',l')
      | Type_tree (t,l) ->
	let t' = do_it t env_l in
	let l' = assoc_pair_loc l env_l in
	Type_tree (t',l')
    end
  in 
  let res = do_it tau.tau env_l in
  build_fragment res tau.effect tau.fexp_loc


(**Unify type and locality of a ginven tagged type*)
(* let unify_tagged_type tau env_t env_l = *)
(*   let final_t = *)
(*     Type_unification.mgu (List.flatten env_t) tau in *)
 
(*   (\*let final_t_env = (List.map (function |TCEmpty -> failwith "TCEmpty"|TCEqual(a,b)|TCEqual_type(a,b) -> (a,b))(List.flatten env_t)) in*\) *)
(*   let final_e_constraints = *)
(*     env_l@[(!gen_loc)] in *)
(*   (\* let unified_loc = Locality_unification.unify_locality (List.flatten final_e_constraints) (List.flatten env_t) (\\*(loc_of_exprtype final_t.tau)*\\) in *\) *)
(*   (\*let final_t_with_l = assoc_locality final_t unified_loc in *)
(*   final_t_with_l;*\) *)
(*   let unified_loc = Locality_unification.unify_locality (List.flatten final_e_constraints) (List.flatten env_t) final_t in *)
(*   failwith "UNIFY_TAGGED_TYPE" *)
  

let type_expression_t expr =
  if !flag_debug then Printf.printf "----Expr----\n%s\n------------\n" (Types.expression_t_to_string expr.expr);
  let free_expr = make_it_free expr in
  if !flag_debug then Printf.printf "----Free Expr----\n%s\n------------\n" (Types.expression_t_to_string free_expr.expr);
  let tau = build_fragment (Type_var ("tau",Multi)) Dot (Tools.fake_location_str "main") in
  let cstr = fst (Constraints.generate_constraints free_expr tau Multi) in
  if !flag_debug then Printf.printf "DAS CSTR: %s\n" (constraints_to_string cstr);
  let {s=_;u;l;c=_} = constraints_solver {s=[CEmpty];u=[];l=[[ECEmpty]]; c=cstr} in
  let flat_gen_loc = List.flatten (List.map (fun x -> !x) (!Type_unification.gen_loc)) in
  let final_e_constraints = l@([flat_gen_loc]) in
  (* gen_loc := []; *)
  let final_t = Unification.unify_tagged_type tau u final_e_constraints in
  final_t.tau
