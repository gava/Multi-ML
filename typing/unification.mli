
val unify_tagged_type : Constraints.fragment ->
  Constraints.t_constraints list list ->
  Constraints.e_constraints list list -> Constraints.fragment
