(**The Multi-ML's "pervasive" … *)

open Constraints
open Types

let __pervasive_multi_env = ref []

let cpt_vars = ref 0

let new_var() =
  incr cpt_vars;
  "δ" ^ string_of_int !cpt_vars


let __add_to_env id value =
  __pervasive_multi_env := (!__pervasive_multi_env)@[(id,value)]

let multi_pervasives_registered id =
  List.assoc id (!__pervasive_multi_env)


(*Redefinition of operators*)

(* (+) operator *)
let _ =
  let l1 = new_level () in
  let l2 = new_level () in
  let l3 = new_level () in
  let l4 = new_level () in
  let l5 = new_level () in
  __add_to_env "+" 
               (build_fragment 
                  (Type_arrow((Type_base("int",Level l1),Dot,Type_arrow((Type_base("int",Level l2),Dot,Type_base("int",Level l3)),Multi)),Multi)) 
	          Dot 
	          (Tools.fake_location_str "op+"))

(* (-) operator *)
let _ =
  __add_to_env "-" 
               (build_fragment 
	          (Type_arrow((Type_base("int",Dot),Dot,Type_arrow((Type_base("int",Dot),Dot,Type_base("int",Dot)),Dot)),Multi)) 
	          Dot 
	          (Tools.fake_location_str "op-"))

(* ( * ) operator *)
let _ =
  __add_to_env "*" 
               (build_fragment 
                  (Type_arrow((Type_base("int",Dot),Dot,Type_arrow((Type_base("int",Dot),Dot,Type_base("int",Dot)),Dot)),Multi)) 
	          Dot 
	          (Tools.fake_location_str "op*"))

(* (=) operator *)
let _ =
  let p1 = new_var () in
  __add_to_env "="
               (build_fragment
	          (Type_arrow((Type_var(p1,Dot),Dot,Type_arrow((Type_var(p1,Dot),Dot,Type_base("bool",Dot)),Dot)),Multi))
	          Dot
	          (Tools.fake_location_str "op="))

(* (^) operator *)
let _ =
  __add_to_env "^"
               (build_fragment 
	          (Type_arrow((Type_base("string",Dot),Dot,Type_arrow((Type_base("string",Dot),Dot,Type_base("string",Dot)),Dot)),Multi)) 
	          Dot 
	          (Tools.fake_location_str "op^"))



(*Multi ML primitives*)

(* (proj) operator  NOT ANY MORE_!*)
(* let _ = *)
(*   let p1 = new_var () in *)
(*   let l1 = new_level () in *)
(*   __add_to_env "proj"  *)
(*     (build_fragment *)
(*        (Type_arrow( *)
(* 	 (Type_par (Type_var (p1,Level l1),Bsp), *)
(* 	  Bsp, *)
(* 	  Type_arrow((Type_base ("int",Dot),Dot,Type_var(p1,Dot)),Dot)), *)
(* 	 Multi)) *)
(*        Dot  *)
(*        (Tools.fake_location_str "op proj")) *)

(* (put) operator *)
let _ =
  let p1 = new_var () in 
  __add_to_env "put" 
               (build_fragment
                  (Type_arrow(
	               (
	                 Type_par(Type_arrow((Type_base("int",Dot),Local,Type_var(p1,Comm)),Dot),Bsp),
	                 Bsp,
	                 Type_par(Type_arrow((Type_base("int",Dot),Local,Type_var(p1,Comm)),Dot),Bsp)
	               ),
	               Multi))
                  Dot 
                  (Tools.fake_location_str "op put"))

(* (mkpar) operator *)
let _ =
  let p1 = new_var () in 
  __add_to_env "mkpar" 
               (build_fragment
                  (
	            Type_arrow((Type_arrow((Type_base("int",Dot),Comm,Type_var(p1,Dot)),Comm),
		                Bsp,
		                Type_par(Type_var(p1,Comm),Bsp)),Bsp)
                  )
                  Dot 
                  (Tools.fake_location_str "op mkpar"))

(* (mktree) operator *)
let _ =
  let p1 = new_var () in
  let l1 = new_level () in
  __add_to_env "mktree" 
               (build_fragment
                  (
	            Type_arrow((Type_var(p1,Level l1),Multi,Type_tree(Type_var(p1,Level l1),Multi)),Multi)
                  )
                  Dot 
                  (Tools.fake_location_str "op mktree"))

(* (pid) operator *)
let _ =
  let p1 = new_var () in
  let l1 = new_level () in
  __add_to_env "pid" 
               (build_fragment
                  (
	            Type_arrow((Type_var(p1,Level l1),Multi,Type_base("int",Dot)),Multi)
                  )
                  Dot 
                  (Tools.fake_location_str "op pid"))


(*OCaml redefinitions*)

(*print_int*) 
let _ =
  let p1 = new_var () in
  let l1 = new_level () in
  __add_to_env "print_int" 
               (build_fragment
                  (
	            Type_arrow((Type_base("int",Level l1),Dot,Type_base("unit",Dot)),Multi)
	          )
	          Dot 
	          (Tools.fake_location_str "op print_int"))

(*print_string*) 
let _ =
  let p1 = new_var () in
  let l1 = new_level () in
  __add_to_env "print_string" 
               (build_fragment
                  (
	            Type_arrow((Type_base("string",Level l1),Dot,Type_base("unit",Dot)),Multi)
	          )
	          Dot 
	          (Tools.fake_location_str "op print_string"))
