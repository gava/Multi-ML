open Location
open Lexing
open Types

exception Unbound_Value of string * Location.t
exception Wrong_Locality of effect * effect * Location.t
exception Wrong_Type of exprtype * exprtype * Location.t
exception Vector_Imbrication of Location.t
exception Non_Communicable of exprtype * Location.t
exception Loc_unification of Types.effect * Types.effect * Location.t
                                           
let handle_unbounded_values s l =
  if l.loc_ghost then
    Printf.printf "\x1b[31mError ! Unbound_value : %s at unknown location(%s)\x1b[0m\n" s (l.loc_start.pos_fname)
  else (
    Printf.printf "\x1b[31mError ! Unbound_value : %s at line %i, characters %i-%i\x1b[0m\n" s (l.loc_start.pos_lnum) (l.loc_start.pos_cnum - l.loc_start.pos_bol) (l.loc_end.pos_cnum - l.loc_end.pos_bol)
  )
  ;exit 1  

let handle_wrong_type given expected l =
  let given = (exprtype_to_string given) in
  let expected = (exprtype_to_string expected) in
  let message = "This expression has type ["^given^"] but an expression was expected of type ["^expected^"]" in
  if l.loc_ghost then
    Printf.printf "\x1b[31mError ! Wrong_Type : %s at unknown location (%s)\x1b[0m\n" message (l.loc_start.pos_fname)
  else 
    Printf.printf "\x1b[31mError ! Wrong_Type : %s at line %i, characters %i-%i\x1b[0m\n" message (l.loc_start.pos_lnum) (l.loc_start.pos_cnum - l.loc_start.pos_bol) (l.loc_end.pos_cnum - l.loc_end.pos_bol)
  ;exit 1

let handle_wrong_locality given expected l = 
  let given = (effect_to_string_full given) in
  let expected = (effect_to_string_full expected) in
  let message = "The locality ["^given^"] is not acceptable in locality ["^expected^"]" in
  if l.loc_ghost then
    Printf.printf "\x1b[31mError ! Wrong_Locality : %s at unknown location (%s)\x1b[0m\n" message (l.loc_start.pos_fname)
  else 
    Printf.printf "\x1b[31mError ! Wrong_Locality : %s at line %i, characters %i-%i\x1b[0m\n" message (l.loc_start.pos_lnum) (l.loc_start.pos_cnum - l.loc_start.pos_bol) (l.loc_end.pos_cnum - l.loc_end.pos_bol)
  ;exit 1

let handle_vector_imbrication l =
  if l.loc_ghost then
    Printf.printf "\x1b[31mError ! Vector_Imbrication : Vector imbrication is not allowed at unknown location (%s)\x1b[0m\n" (l.loc_start.pos_fname)
  else 
    Printf.printf "\x1b[31mError ! Vector_Imbrication : Vector imbrication is not allowed at line %i, characters %i-%i\x1b[0m\n" (l.loc_start.pos_lnum) (l.loc_start.pos_cnum - l.loc_start.pos_bol) (l.loc_end.pos_cnum - l.loc_end.pos_bol)
  ;exit 1

let handle_non_communicable e l = 
  if l.loc_ghost then
    Printf.printf "\x1b[31mError ! Non_Communicable : The type %s is non communicable at unknown location (%s)\x1b[0m\n" (exprtype_to_string e) (l.loc_start.pos_fname)
  else 
    Printf.printf "\x1b[31mError ! Non_Communicable : The type %s is non communicable at line %i, characters %i-%i\x1b[0m\n" (exprtype_to_string e) (l.loc_start.pos_lnum) (l.loc_start.pos_cnum - l.loc_start.pos_bol) (l.loc_end.pos_cnum - l.loc_end.pos_bol)
  ;exit 1

let handle_loc_unification e1 e2 l =
  if l.loc_ghost then
    Printf.printf "\x1b[31mError ! Loc_unification : The localities %s and %s are not compatible at unknow location (%s) \x1b[0m\n" (effect_to_string e1) (effect_to_string e2) (l.loc_start.pos_fname)
  else 
    Printf.printf "\x1b[31mError ! Loc_unification : The localities %s and %s are not compatible at line %i, characters %i-%i\x1b[0m\n" (effect_to_string e1) (effect_to_string e2) (l.loc_start.pos_lnum) (l.loc_start.pos_cnum - l.loc_start.pos_bol) (l.loc_end.pos_cnum - l.loc_end.pos_bol)
  ;exit 1
