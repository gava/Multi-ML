open Parsetree
open Asttypes
open Lexing

exception Unknown_case of string

(*Pour générer un nouveau pattern fake*)
let fake_location = 
  let loc = {pos_fname = "Fake";pos_lnum = 1;pos_bol = 0;pos_cnum = -1;} in
  { Location.loc_start = loc; loc_end = loc; loc_ghost = true }

(*Pour générer un nouveau pattern fake*)
let fake_location_str str = 
  let loc = {pos_fname = str;pos_lnum = 1;pos_bol = 0;pos_cnum = -1;} in
  { Location.loc_start = loc; loc_end = loc; loc_ghost = true }

let unsome = function Some a -> a | None -> raise Not_found

let mk_exp a b = 
  {pexp_desc = a; pexp_loc = b;pexp_attributes = []}

let mk_pexp_open_vector a b = 
  Pexp_open_vector ({txt=Longident.Lident a;loc=b})

let mk_pexp_ident a b =
  Pexp_ident ({txt=Longident.Lident a;loc=b})

let mk_patt a b = 
  {ppat_desc=a;ppat_loc=b;ppat_attributes=[]}

let mk_ppat_var a b = 
  Ppat_var{txt=a;loc=b}

let mk_pvt a b c d =
  {
    pvb_pat = a;
    pvb_expr = b;
    pvb_attributes = c;
    pvb_loc = d;
  }

let mk_parsetree_struct_item a b = 
  {pstr_desc= a; pstr_loc= b}

let rec lident_to_string li =
  match li with
  | Longident.Lident s -> s
  | Longident.Ldot (t,s) -> (lident_to_string t)^"."^s
  | _ -> raise (Unknown_case "lident_to_string")

let core_type_to_string t =
  begin
    match t.ptyp_desc with
      Ptyp_constr (li,_) -> 
	lident_to_string li.txt
    | _ -> "undefined"
  end


(*Annotation de type tree : nécessaire sans le typge …*)
let type_tree_table : (string,string) Hashtbl.t = Hashtbl.create 1000

let rec ppat_var_to_str p =
  match p.ppat_desc with
  (* | Ppat_any -> "" *)
  | Ppat_tuple l -> ignore(List.map (ppat_var_to_str) l) ;""
  | Ppat_var e -> e.txt
  | Ppat_construct _ -> "()"
  | Ppat_constraint (pat,typ) ->
    let str_var = ppat_var_to_str pat in
    let str_type = core_type_to_string typ in
    if str_type = "tree" then
      Hashtbl.replace type_tree_table str_var str_type;
    Printf.printf "[TYPING ANNOTATION]var [%s] is typed [%s]\n" str_var str_type;
    str_var
  | _ -> failwith "Match fail: ppat_var_to_str"

let is_pervasive_op = 
  function 
  |"*"|"**"|"*."|"!"|"!="|"&&"|"&"|"+"|"+."|"-"|"-."|"/"|"/."|":="|"<"|"<="|"<>"|"="|"=="|">"|">="|"@"|"@@"|"^"|"^^"|"asr"|"land"|"lor"|"lsl"|"lsr"|"lxor"|"mod"|"or"|"|>"|"||"|"~+"|"~+."|"~-"|"~-." (*to be continued*) -> true
  | _ -> false
