open Parsetree
open Lexing
open Tools
open Asttypes

exception Unknown_case of string

let __debug = ref true

let debug s = if !__debug = true then print_string (s^"\n") 

(*Pour générer un nouveau pattern fake*)
let fake_location = 
  let loc = {pos_fname = "Fake";pos_lnum = 1;pos_bol = 0;pos_cnum = -1;} in
  { Location.loc_start = loc; loc_end = loc; loc_ghost = true }

let pid__arg = mk_ppat_var "pid__arg" fake_location

(*Générateur d'arbres frais !*)
let tmp_tree_id = ref (-1)
let get_tmp_tree_id i = 
    tmp_tree_id := !tmp_tree_id +i ;
  "__tmp_tree_"^(string_of_int (!tmp_tree_id))


(*Générateur de noms de variables fraîches*)
let var_name = ref 0
let fresh cpt =
  let v = !cpt in
  cpt := !cpt + 1;
  "gen__"^(string_of_int v);;

let (multi_table : string list ref) = {contents = []}


let def_tree = ref false
let (assoc_val_to_tmp_tree : (string * string) list ref)= {contents = []}
(*Pour redéfinir les variables dans replicate*)
let need_redef = ref false
let (redefs : (pattern * expression) list ref)  = {contents = []}
let (mfun_redef : string list ref) = {contents = []}

let str_to_pexp str = 
  let lexbuf = Lexing.from_string str in 
  try
    let res = Parse.implementation lexbuf in
    let pstr = (List.hd res).pstr_desc in 
    begin 
      match pstr with 
      |	Pstr_eval (e,_) -> e
      | Pstr_value (_,bl) -> (List.hd bl).pvb_expr
      (*| Pstr_value (rf,pel) -> snd (List.hd pel)*)
      | _ -> failwith("TODO : str_to_pexp")
    end
  with
  | _ -> failwith ("Error (str_to_pexp) while parsing: "^str)

let pexp_to_str pe =
  let fe = mk_exp pe fake_location in
  Pprintast_seq.string_of_expression fe

(* {pstr_desc= Pstr_value(rf,res);pstr_loc=a.pstr_loc;} *)

(*
  [
  structure_item ([1,0+0]..[1,0+10])
  Pstr_value Nonrec
  [
  <def>
  pattern ([1,0+4]..[1,0+5])
  Ppat_var "x" ([1,0+4]..[1,0+5])
  expression ([1,0+8]..[1,0+10])
  Pexp_constant Const_int 42
  ]
  ]
*)

(*| (Lexer.Error(Lexer.Unterminated_comment _, _) )->
  print_string "Unterminated_comment\n"
  ; failwith("Error")
  | Syntaxerr.Error e->
  print_string "Syntax Error\n";
  Syntaxerr.report_error Format.err_formatter e
  ; failwith("Error")
  (*TODO : position, ligne, etc.*)
  | Parsing.Parse_error->
  print_string "Parsing Error\n";
  let loc = Location.curr lexbuf in
  ignore (raise(Syntaxerr.Error(Syntaxerr.Other loc)))
  ; failwith("Error")
  | _ -> failwith("str_to_pexp : error … C'est pas trop normal !")*)


let rec apply_through exp f = 
  begin
    match exp.pexp_desc with
    | Pexp_let (_,_,ein) -> 
      [f ein]
    | Pexp_apply (a1,a2) -> 
      ([f a1])@(List.flatten (List.map (fun x ->(apply_through (snd x) f)) a2))
    | Pexp_copy _ ->
      [f exp]
    | Pexp_open_vector _ ->
      [f exp]
    | Pexp_ident _ ->
      [f exp]
    | Pexp_constant _ ->
      [f exp]
    | _ -> failwith "Match fail: apply_through"
  end

let rec texpr in_ast = 
  let res = 
    begin
      match in_ast.pexp_desc with 
      (*Multi-ML*)      

      | Pexp_multi_function (_,pe) -> debug "Pexp_multi_function"; 

	let rec unwrap_multi arg lst =
	  begin
	    match (fun x -> x.pexp_desc ) (snd (List.hd arg)) with 

	    | Pexp_multi_function (_,pe) ->
	      let new_patt = mk_patt (mk_ppat_var (ppat_var_to_str (fst (List.hd arg))) fake_location) fake_location in
	      let res = unwrap_multi pe (lst@[(ppat_var_to_str (fst (List.hd arg)))]) in
	      mk_exp
		(Pexp_fun(
		  "",
		  None,
		  new_patt,
		  res))
		fake_location

	    | Pexp_wherebody (e1,e2) -> debug "Pexp_wherebody";
	      let new_cond = str_to_pexp "node ()" in
	      let new_if = texpr e1 in
	      let new_else = Some (texpr e2) in
	      let cond = mk_exp (Pexp_ifthenelse(new_cond,new_if,new_else)) fake_location in
	      let new_patt = mk_patt (mk_ppat_var (ppat_var_to_str (fst (List.hd arg))) fake_location) fake_location in
	      mk_exp
		(Pexp_fun(
		  "",
		  None,
		  new_patt,
		  cond))
		fake_location

	    | _ -> failwith "Match fail : unwrap_multi"

	  end
	in
	let final_fun = unwrap_multi pe [] in
	final_fun.pexp_desc

      (*BSML*)
      | Pexp_replicate e -> debug "Pexp_replicate";
	let new_patt = mk_patt pid__arg fake_location in
	let new_expr = mk_exp (Pexp_fun ("",None,new_patt,texpr e)) in_ast.pexp_loc in 
	let out_expr = Pexp_replicate new_expr in
	if !need_redef = true then
	  (
	    debug ("Need"^(string_of_int (List.length !redefs))^"redefs\n");
	    need_redef := false;
	    let rec fct lst =
	      begin
		match lst with
		| h::t -> 
		  let vb = mk_pvt (fst h) (snd h) [] fake_location in
		  Pexp_let (Nonrecursive,
			    [vb],
			    mk_exp (fct t) in_ast.pexp_loc)
		| [] -> out_expr
	      end
	    in 
	    let new_def = fct !redefs in
	    redefs := [];
	    new_def
	  )
	else 
	  out_expr
  
      | Pexp_mkpar e -> debug "Pexp_mkpar";
	Pexp_mkpar (texpr e)

      | Pexp_proj e -> debug "Pexp_proj";
	Pexp_proj (texpr e)

      | Pexp_put e -> debug "Pexp_put";
	Pexp_put (texpr e)
	  
      | Pexp_open_vector e -> debug "Pexp_open_vector";
	Pexp_open_vector e

      | Pexp_copy e -> debug "Pexp_copy";
	(*original id var*)
	let e_name = lident_to_string e.txt in
	(*generated id var*)
	let s = fresh var_name in
	(*Pattern for generated var*)
	let new_ppat_var = mk_ppat_var s fake_location in 
	(*New pattern*)
	let patt = mk_patt new_ppat_var fake_location in
	(*New desc*)
	let desc = mk_exp (mk_pexp_ident e_name (e.loc)) fake_location in
	(*Build fun in order to replicate the generated var*)
	let new_patt_fun = mk_patt Ppat_any fake_location in
	let new_expr_fun = mk_exp (Pexp_fun ("",None,new_patt_fun,desc)) fake_location in 
	(*Final experssion*)
	let tab_expr = (patt, (mk_exp (Pexp_replicate(new_expr_fun)) fake_location)) in
	(*Say : we need to insert some generated bindings*)
	need_redef := true;
	(*Add those bindings*)
	redefs := !redefs@([tab_expr]);
	mk_pexp_open_vector s (e.loc)

      (* | Pexp_glo e -> debug "Pexp_glo"; *)
      (* 	need_redef:=true; *)
      (* 	let s = lident_to_string (e.txt) in *)
      (* 	redefs :=  *)
      (* 	  ([mk_patt (mk_ppat_var s fake_location) fake_location,in_ast])@(!redefs); *)
      (* 	Printf.printf "====> %s\n" s; *)
      (* 	Pexp_glo e *)

      | Pexp_finally (e1) ->
         debug "Pexp_finally";
         failwith "TODO : Pexp_finally"
         (*let final_exp =
           (function | Some x ->
                        Pexp_tuple [texpr x;texpr e2]
                     | None ->
                        Pexp_tuple [texpr e2]) e1
         in
         
         final_exp*)

      | Pexp_mktree e -> debug "Pexp_mktree";
	                 let name = "__fake_multi " in
	                 def_tree := true;
	let str = "let multi "^name^" () ="^
	  "where node = "^
	  "let _ = << "^name^" () >> in "^
	  "let __res = "^pexp_to_str e.pexp_desc ^" in"^
	  " finally ~up:() ~keep:__res "^
	  "where leaf = "^
	  "let __res = "^pexp_to_str e.pexp_desc ^" in "^
	                   "finally ~up:() ~keep:__res "^
	                 "in (__fake_multi ();"^(snd (List.hd !assoc_val_to_tmp_tree))^")" in                   
	(* let fake_patt = mk_patt (mk_ppat_var "__fake_multi" fake_location) fake_location in *)
	(* let body = mk_exp (Pexp_constant(Const_int 42)) fake_location in *)
	(* let wherebody = mk_exp (Pexp_wherebody(body,body)) fake_location in *)
	(* let out_multi = Pexp_multi_function("",[fake_patt,wherebody]) in *)
	(* (texpr (mk_exp out_multi fake_location)).pexp_desc; *)
	multi_table:= (!multi_table)@([name]);
	let res = texpr (str_to_pexp str) in
	def_tree := false;
	res.pexp_desc

      (*Ocaml*)
      | Pexp_let (rf,vbl,e_in) -> debug "Pexp_let";
	let new_vb = List.map (fun x -> mk_pvt x.pvb_pat (texpr x.pvb_expr) x.pvb_attributes x.pvb_loc) vbl in
	let new_ein = texpr e_in in
	let new_let = Pexp_let (rf,new_vb,new_ein) in
	begin
	  match rf with
	  | Multirecursive -> debug "yep c'est une multi function !!"
	  | _ -> ()
	end;
	new_let

      | Pexp_fun (s,r,p,e) -> debug "Pexp_fun";
	let new_e = texpr e in
	Pexp_fun (s,r,p,new_e)

      | Pexp_apply (a1,a2) ->  debug "Pexp_apply";
	let new_a1 = texpr a1 in
	let new_a2 = List.map (fun x -> (fst x, texpr (snd x))) a2 in
	Pexp_apply (new_a1,new_a2)

      | Pexp_ident i -> debug "Pexp_ident";
	begin
	  try
	    match List.find (fun a -> a = (lident_to_string i.txt)) !multi_table with
	    | _ -> raise Not_found
	      (*TODO : remonter des paires comme il faudrait*)
	     (* let tr = str_to_pexp (snd (List.hd !assoc_val_to_tmp_tree)) in
	      Pexp_tuple [mk_exp (Pexp_ident i) fake_location;tr]*)
	      (*let s = lident_to_string i.txt in
	      debug ("idente :"^(lident_to_string i.txt)^"\n");
	      mfun_redef := (!mfun_redef)@([s]);
	      (texpr (mk_exp (Pexp_loc i) in_ast.pexp_loc)).pexp_desc;*)
	  with 
	  | Not_found -> 
	      debug ("ident :"^(lident_to_string i.txt)^"\n");
	    Pexp_ident i
	  | _ -> failwith "Match fail: Pexp_ident"
	end

      | Pexp_tuple e -> debug "Pexp_tuple";
	Pexp_tuple (List.map texpr e)

      | Pexp_sequence (e1,e2) -> debug "Pexp_sequence";
	Pexp_sequence (texpr e1,texpr e2)

      | Pexp_ifthenelse (e1,e2,eo) -> debug "Pexp_ifthenelse";
	let new_eo =
	  begin
	    match eo with 
	    | Some e -> Some (texpr e)
	    | None -> None
	  end
	in
	Pexp_ifthenelse (texpr e1, texpr e2, new_eo)

      | Pexp_constant c -> debug "Pexp_constant"; 
	Pexp_constant c

      | Pexp_construct (e,f) -> debug "Pexp_construct";
	Pexp_construct (e,f)
	  
      | Pexp_array ar -> debug "Pexp_array";
	Pexp_array ar
	  
      | _ -> raise (Unknown_case"Tiens, t'as encore oublié un Pexp !! ;)")
    end
  in {pexp_desc=res;pexp_loc=in_ast.pexp_loc;pexp_attributes=[]}


let rec check_multi_fun lst = 
  begin
    match lst with
    | h::t ->
      begin 
	match h with
	| (p,_) -> print_endline (ppat_var_to_str p)
      end ;
      check_multi_fun t
    | [] -> ()
  end

let is_multi_fun e =
  begin
    match (snd e).pexp_desc with 
    | Pexp_multi_function _ -> 
      let tmp_name = get_tmp_tree_id 1 in
      assoc_val_to_tmp_tree := 
	[ppat_var_to_str (fst e),tmp_name]@(!assoc_val_to_tmp_tree);
      true
    | Pexp_mktree _ -> 
      let tmp_name = get_tmp_tree_id 1 in
      assoc_val_to_tmp_tree := 
	[ppat_var_to_str (fst e),tmp_name]@(!assoc_val_to_tmp_tree);
      true
    | _ -> false
  end

let init_multi_exp str loc =
  let pat_id = mk_patt (mk_ppat_var str (fake_location)) fake_location in
  let id = str_to_pexp ("__init_tree __archi_level") in
  let id_pvt = mk_pvt pat_id id [] loc in
    [{pstr_desc= Pstr_value(Nonrecursive,[id_pvt]);pstr_loc=loc}]

let run_transform a =
  begin
    match a.pstr_desc with
    | Pstr_value (rf,vbl) -> debug "\nPstr_value";
      let pel = (List.map (fun x -> (x.pvb_pat,x.pvb_expr)) vbl) in
      let name = ppat_var_to_str (fst (List.hd pel)) in
      let is_mf = is_multi_fun (List.hd pel) in
      if is_mf then (
	debug ("Multi binding =>> "^name^"\n");
	multi_table:= (!multi_table)@([name]);
	let res = List.map (fun (x) -> 
	  mk_pvt x.pvb_pat (texpr x.pvb_expr) x.pvb_attributes x.pvb_loc 
	) vbl in
	(*init fake multi N*)
	let init_multi = init_multi_exp (snd (List.hd !assoc_val_to_tmp_tree)) (a.pstr_loc) in
	(init_multi)@[{pstr_desc= Pstr_value(rf,res);pstr_loc=a.pstr_loc;}])
      else (
	let res = List.map (fun (x) -> 
	  mk_pvt x.pvb_pat (texpr x.pvb_expr) x.pvb_attributes x.pvb_loc 
	) vbl in
	[{pstr_desc= Pstr_value(rf,res);pstr_loc=a.pstr_loc;}]) 

    
    
    | Pstr_eval (e,attr) -> debug "\nPstr_eval";
      let res = texpr e in 
      [{pstr_desc= Pstr_eval(res,attr);pstr_loc=a.pstr_loc;}]
    | _ -> failwith "Match fail: run_transform"
  end

let transform in_ast = 
  let res = List.flatten (List.map run_transform in_ast) in
  debug "Transformation succeed !"; res
