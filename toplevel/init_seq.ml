exception No_value
exception Invalid_globad_identifier

let __debug = ref false

let debug () =
  if !__debug then (
    __debug := not (!__debug);
    Printf.printf "Debug Off !\n"
  )
  else(
    __debug := not (!__debug);
    Printf.printf "Debug On !\n"
  )

type 'a value = Value of 'a
		| Invalid of exn

type 'a tree = Node of ('a value ref * 'a tree list)
		    | Leaf of 'a value ref

type gid = int list

type pid = int

type gid_p = (gid * pid * int)


let __archi_level : int tree =
  (*Processeur*)
  Node ( ref (Value 2) , [
    (*Cœur0 avec 2 threads*)
    Node (ref ( Value 1) , [ Leaf( ref (Value 0));Leaf(ref (Value 0 )) (*;Leaf( () ) ;Leaf( () ) *)])
    ;
    (*Cœur1 avec 2 threads*)
    Node (ref (Value 1) , [Leaf( ref (Value 0));Leaf(ref (Value 0) ) ])
  ])

let rec string_from_gid gid =
  begin
    match gid with
    | h::[] -> string_of_int h
    | h::t -> ((string_of_int h)^(".")^(string_from_gid t))
    | _ -> failwith "Error : string_from_gid"
  end

let go_down n g = 
  if !n = 0 then failwith ("C'est pas possible mon petit !");
  if !__debug then Printf.printf "Going down to : %s\n" (string_of_int (!n-1));
  n := !n - 1;
  g := (!g)@[0]

let go_up n g = 
  if !__debug then Printf.printf "Going up to :%s\n" (string_of_int (!n+1));
  n := !n + 1;
  g := List.rev (List.tl (List.rev !g))

let sons lvl =
  let rec fct tree =
    begin
      match tree with
      | Node (v,s) ->
	if !v = Value lvl then
	  List.length s
	else fct (List.hd s)
      | Leaf v -> 0
    end
  in Array.to_list (Array.init (fct __archi_level) (fun i -> i))

(*Change le gid courant*)
let gid_proj c (i :int) =
  let tl = List.rev (List.tl (List.rev !c)) in
  c := (tl@[i])

let __print_tree (t : int tree) = 
  let rec job n depth gid =
    begin 
      match n with
      | Node (v,s) ->
	Printf.printf "o \"";
	let v = (function | Value a -> a | _ -> raise No_value) !v in
	Printf.printf "%s -> %i" (string_from_gid gid) v;
	for i = 0 to depth do
	  Printf.printf " "
	done;
	Printf.printf "\"\n";
	ignore (List.mapi (fun i x -> job x (depth + 1) (gid@[i]) ) s);()
      | Leaf (v) ->
	for i = 1 to depth do
	  Printf.printf "| "
	done;
        let v = (function | Value a -> a | _ -> raise No_value) !v in
	Printf.printf "\"%s -> %i" (string_from_gid gid) v;
      Printf.printf "\"\n"
  end
  in job t 0 [0]

let __print_tree (t : string tree) = 
  let rec job n depth gid =
    begin 
      match n with
      | Node (v,s) ->
	Printf.printf "o ";
	let v = (function | Value a -> a | _ -> raise No_value) !v in
	Printf.printf "\"%s -> %s" (string_from_gid gid) v;
	for i = 0 to depth do
	  Printf.printf " "
	done;
	Printf.printf "\n";
	ignore (List.mapi (fun i x -> job x (depth + 1) (gid@[i]) ) s);()
      | Leaf (v) ->
	for i = 1 to depth do
	  Printf.printf "| "
	done;
	let v = (function | Value a -> a | _ -> raise No_value) !v in
	Printf.printf "\"%s -> %s" (string_from_gid gid) v;
	Printf.printf "\"\n"
    end
  in job t 0 [0]


let set_tree_at tree value gid =
  let work_done = ref false in
  let rec aux tree value gid depth =
    begin 
      match tree with
      | Node (d,s) -> 
	let g = string_from_gid depth in
	begin 
	  match !d with
	  | Invalid e -> 
	    if g = gid then (
	      if !__debug then Printf.printf "N I'm going to set %s\n" gid;
	      work_done := true;
	      d := Value value
	    )
	  | Value (v) ->
	    if g = gid then
	      (
		if !__debug then Printf.printf "N I'm going to set %s\n" gid;
		work_done := true;
		d := Value value)
	end ;
	ignore(List.mapi (fun i x -> aux x value gid (depth@([i]))) s)
      | Leaf d -> 
	let g = string_from_gid depth in
	begin 
	  match !d with
	  | Invalid e -> 
	    if g = gid then (
	      if !__debug then Printf.printf  "N I'm going to set %s\n" gid;
	      work_done := true;
	      d := Value value
	    )
	  | Value (v) ->
	    if g = gid then
	      ( if !__debug then Printf.printf "L I'm going to set %s\n" gid;
		 work_done := true;
		 d := Value value)
	end
    end
  in aux tree value gid [0]; 
  (if !work_done = false then raise Invalid_globad_identifier)

(*C'est juste un arbre de test à retirer plus tard !*)
let tr = 
Node (ref(Value 42),
[
Node (ref(Value 0),[Leaf (ref(Value 1));  Leaf (ref(Value 2))]);
Node (ref(Value 0),[Leaf (ref(Invalid No_value));  Leaf (ref(Value 2))])]
)

(*Initialisation d'un arbre avec des No_value*)
let rec __init_tree (archi : int tree ) = 
  begin 
    match archi with
    | Node (_,s) ->
      Node (ref (Invalid No_value), List.map (fun x -> __init_tree x) s)
    | Leaf (_) ->
      Leaf (ref (Invalid No_value))
  end


(*Initialisation d'un arbre avec un valeur*)
let rec __init_tree_f (archi : int tree ) f = 
  begin 
    match archi with
    | Node (_,s) ->
      Node (ref (Value (f ())), List.map (fun x -> __init_tree_f x f) s)
    | Leaf (_) ->
      Leaf (ref (Value (f ())))
  end


let rec __build_global_var f =
  let rec aux tree depth =
    begin 
      match tree with
      | Node (d,s) ->
	let g = string_from_gid depth in 
	print_endline g;
	let res = List.mapi (fun i x -> aux x (depth@[i])) s in
	let its_me_marrrrrio = [g,f] in
	List.fold_left (fun x y -> x@y ) its_me_marrrrrio res
      | Leaf (d) -> 
	let g = string_from_gid depth in 
	print_endline g;
	[(g,f)]
    end
  in aux __archi_level [0]


(*Profondeur de l'arbre*)
let tree_depth = 3;;
(*Nœud courant*)
let mbsml_node = ref 2;;
(*Gid courant*)
let mbsml_gid = ref [0];;


let __gid () = 
  (!mbsml_gid, List.hd (List.rev !mbsml_gid), List.length (sons !mbsml_node))

(*gid au niveau Multi-BSP*)
let gid ()=
  string_from_gid (!mbsml_gid)

(*pid au niveau BSP*)
let pid () = 
  List.hd (List.rev !mbsml_gid)

(*Nb procs du niveau*)
let nprocs () = 
  sons !mbsml_node

let replicate e = 
  if !__debug then Printf.printf "*replicate at %s\n" (string_from_gid !mbsml_gid);
  let s = (sons !mbsml_node) in
  go_down mbsml_node mbsml_gid;
  let res = List.map (fun i -> gid_proj mbsml_gid i; e i) s in
  go_up mbsml_node mbsml_gid;
  res

let mkpar f =
  if !__debug then Printf.printf "*mkpar at %s\n" (string_from_gid !mbsml_gid);
  let s = (sons !mbsml_node) in
  go_down mbsml_node mbsml_gid;
  let res = List.map f s in
  go_up mbsml_node mbsml_gid;
  res

let proj e  = 
  if !__debug then Printf.printf "*proj at %s\n" (string_from_gid !mbsml_gid);
  List.nth e 

let put vf = 
  if !__debug then Printf.printf "*put at %s\n" (string_from_gid !mbsml_gid);
  Array.to_list (Array.init (List.length (sons !mbsml_node)) (fun i -> fun j -> (List.nth vf j) i))

let node () = if !mbsml_node <> 0 then true else false

let own i lst = List.nth lst i

let generated_finally (up,keep,tree) =
  let cg = gid() in
  if !mbsml_node = 0 then
    ( set_tree_at tree keep cg ;
      if !__debug then Printf.printf "Keep and set leaf %s\n" cg)
  else
    if !mbsml_node = (tree_depth-1) then
      (set_tree_at tree keep cg ;
       if !__debug then Printf.printf "Keep node %s and build final tree \n" cg)
    else
      (set_tree_at tree keep cg ;
       if !__debug then Printf.printf "Keep node %s\n" cg)
  ;up

(*
  (*Useless ??
Change to gid ()*)
let rec read_tree tree = 
  let out = ref (Obj.magic None) in
  begin
    match tree with
    | Node (v,s) ->
      let g = (fun (x,_,_)  -> x) ( __gid () ) in
      if g = !mbsml_gid then
	out := v
      else ignore(List.map (fun x -> read_tree x) s)
    | Leaf v ->
      let g = (fun (x,_,_)  -> x) ( __gid () ) in
      if g = !mbsml_gid then
	out := v
  end;
  (function | Value v -> v | Invalid e -> raise e) (!(!out))
*)

let print_string_tree format t =
  Format.pp_open_box  format 0;
  Format.pp_print_string format "\n";
    let rec job n depth gid =
    begin 
      match n with
      | Node (v,s) ->
	for i = 0 to (depth-(if depth = 0 then 1 else 0)) do
	  Format.pp_print_string format "-"
	done;
	Format.pp_print_string format "o ";
	let v = (function | Value a -> a | _ -> raise No_value) !v in
	(* Format.pp_print_string format (string_from_gid gid); *)
	Format.pp_print_string format ("\""^v^"\"");
	if (depth ) = 0 then 
	  Format.pp_print_string format "\n|";
	Format.pp_print_string format "\n";
	ignore (List.mapi (fun i x -> job x (depth + 1) (gid@[i]) ) s);()
      | Leaf (v) ->
	for i = 1 to depth do
	  Format.pp_print_string format "| "
	done;
	  Format.pp_print_string format "-";
	let v = (function | Value a -> a | _ -> raise No_value) !v in
	(* Format.pp_print_string format (string_from_gid gid); *)
	Format.pp_print_string format ("-> \""^v);
	Format.pp_print_string format "\"\n"
    end
  in job t 0 [0];
  Format.pp_close_box  format ();;

#install_printer print_string_tree

let print_int_tree format t =
  Format.pp_open_box  format 0;
  Format.pp_print_string format "\n";
    let rec job n depth gid =
    begin 
      match n with
      | Node (v,s) ->
	for i = 0 to (depth-(if depth = 0 then 1 else 0)) do
	  Format.pp_print_string format "-"
	done;
	Format.pp_print_string format "o ";
	let v = (function | Value a -> a | _ -> raise No_value) !v in
	(* Format.pp_print_string format (string_from_gid gid); *)
	Format.pp_print_int format (v);
	if (depth ) = 0 then 
	  Format.pp_print_string format "\n|";
	Format.pp_print_string format "\n";
	ignore (List.mapi (fun i x -> job x (depth + 1) (gid@[i]) ) s);()
      | Leaf (v) ->
	for i = 1 to depth do
	  Format.pp_print_string format "| "
	done;
	  Format.pp_print_string format "-";
	let v = (function | Value a -> a | _ -> raise No_value) !v in
	(* Format.pp_print_string format (string_from_gid gid); *)
	Format.pp_print_string format ("-> ");
	Format.pp_print_int format v;
	Format.pp_print_string format "\n"
    end
  in job t 0 [0];
  Format.pp_close_box  format ();;

#install_printer print_int_tree


(*Fonction identité, contrairement à la version MPI ou il faut être explicite sur le type tree*)
let at x = x
