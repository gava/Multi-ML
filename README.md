Multi-ML
===================

**This is a beta version. Good luck with it ;)**

What is Multi-ML?
-----------

Multi-ML is a programming language implemented over OCaml that combines the high degree of abstraction of ML with the scalable and predictable performances of multi-BSP. The multi-BSP model is an extension of the well-known BSP bridging model. It brings a tree-based view of nested components to represent hierarchical architectures. In the past, we designed BSML for programming BSP algorithms in ML. We now propose the Multi-ML language as an extension of BSML for programming Multi-BSP algorithms in ML. 

More information about the language can be found here:
https://www.lacl.fr/~vallombert/articles/Victor_Allombert_Thesis.pdf

----------

Compile the Multi-ML language
-------------

To build Multi-ML:
`$ make`

To build the documentation :
`$ make doc`

Then, the compilers can be found in `bin/`

> **Requierd packages are :**

> - OCaml 4.02.1

----------

Compile a Multi-ML code
-------------

To compile a Multi-ML code for a parallel machine:
`mmlopt.mpi [OPTION] code.mml`

----------

Run a Multi-ML code
-------------

To run a Multi-ML code for a parallel machine:
`mpirun -np $NPROCS ./exec`

----------

*… To be continued …*